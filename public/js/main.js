console.log("zdravo 2")

// koristi se https://leafletjs.com/

// Koordinate traziti ovde https://www.openstreetmap.org/#map=12/45.3706/20.3958
// to treba da pise u korisnickom interfejsu administratroa
// lapovo geo:44.1855,21.1047?z=12

podesavanja = {
    putanja_do_ikonice_novcica: '/img/novcic1.png',
    zrenjanin_latitude: 44.1855,
    zrenjanin_longitude: 21.1047,
    zrenjanin_zoom: 12,
    lapovo_latitude: 44.1855,
    lapovo_longitude: 21.1047,
    lapovo_zoom: 13,
    test: "bla bla bla"
};


// var oldpopupcontent = '<a href="https://google.com/">Hello world!</a><br>I am a popup.';
var foto_url_primer = 'https://dobbia.com/assets/frontends/img/testimonial-1.png';
var click_url_primer = 'https://duckduckgo.com/';
var naslov_primer = 'Neki naslov';
// var fotocontent = '<p>Neki naslov</p><br><img src="' + foto_url + '">';

var popup_slika_placeholder = ''; // ovde mozes da stavis url placeholder slike


/*
activeProjectPhotos.forEach(listActiveProjectPhotos);
function listActiveProjectPhotos(item, index) {
    popup_slika_placeholder = '/images/' + item;
    console.log(item);
}
*/

function template_za_popup(foto_url, naslov) {
    var foto_html = popup_slika_placeholder;
    if (typeof foto_url === 'string' && foto_url !== '') {
        // ako je poslata slika
        foto_html = '<img style="height: 100px; width: 100px;" class="map-popup-slicica" src="' + foto_url + '">'
    } else {
        // ako nema slike
        if (popup_slika_placeholder !== '') {
            // ako ima placeholder slika
            foto_html = '<img style="height: 100px; width: 100px;" class="map-popup-slicica" src="' + popup_slika_placeholder + '">'
        }
    }
    var naslov_html = '';
    if (typeof naslov === 'string' && naslov !== '') {
        naslov_html = naslov;
    }
    var html = '<div class="moj-map-popup"><p class="map-popup-naslov clearfix">' + naslov_html + '</p>' + foto_html + '</div>';
    return html;
}

function otvori_link_u_novom_tabu(url) {
    console.log('Otavaramo link: ', url);
    window.open(url, '_blank');
}

function click_na_marker(url) {
    // currying - funkcija koja vraca funkciju
    function pozovi_me_na_klik() {
        // currzing omocava da argument gornje funkcija ostane da zivi u kontekstu ove funkcije
        // tako da sada moyemo da prikacimo poseban url za svaki click event
        otvori_link_u_novom_tabu(url);
    }
    return pozovi_me_na_klik;
}

function testclicked() {
    // alert('Kliknuli smo na marker!');
    console.log('Kliknuli smo na marker!');
}
function testhovered() {
    console.log('HOVERUJEMO');
    // alert('Hoverujemo na marker!');
}

// functions

function sveSpremnoZaMape() {
    console.log("pozvali smo sveSpremnoZaMape() ");
    pocinjemo();
}

var data_za_novcic = {};
var data_za_vise_novcica = [];
function pokupiPodatke() {
    // prikupljamo podatke koje je PHP ostavio u template
    // pokupiPodatkeZaJedan();
    pokupiPodatkeZaVise();
}
function pokupiPodatkeZaJedan() {
    // u slucaju da ima samo jedan novcic
    data_za_novcic.znamo_lokaciju = window.znamo_lokaciju;
    data_za_novcic.novcic_latitude = window.novcic_latitude;
    data_za_novcic.novcic_longitude = window.novcic_longitude;
}
function pokupiPodatkeZaVise() {
    // u slucaju da ima samo jedan novcic
    data_za_vise_novcica = window.vise_novcica;
}

function pocinjemo() {
    console.log("test pocinjemo");

    pokupiPodatke(); // pokupi podatke koje je PHP ostavio u template
    var mymap = L.map('mapid').setView([podesavanja.lapovo_latitude, podesavanja.lapovo_longitude], podesavanja.lapovo_zoom);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(mymap);

    var novcicIcon = L.icon({
        iconUrl: podesavanja.putanja_do_ikonice_novcica,
        iconSize: [40, 40],
        iconAnchor: [22, 94],
        popupAnchor: [-3, -76],
    });

    data_za_vise_novcica.forEach(function (item) {
        console.log('item');
        console.log(item);
        if (item.znamo_lokaciju && item.znamo_lokaciju !== "undefined") {
            var marker = L.marker([item.novcic_latitude, item.novcic_longitude], {
                icon: novcicIcon
            }).addTo(mymap);

            var link_za_klik = item.novcic_click_otvori_url;

            // click_na_marker(url)
            marker.on('click', click_na_marker(link_za_klik)); // currying

            // marker.on('click', testclicked); // test
            marker.on('mouseover', testhovered); // test

            var slika_za_popup = item.novcic_popup_slika;
            var naslov_za_popup = item.novcic_popup_naslov;
            marker.bindPopup(template_za_popup(slika_za_popup, naslov_za_popup));
            marker.on('mouseover', function (e) {
                this.openPopup();
            });
            marker.on('mouseout', function (e) {
                this.closePopup();
            });
        }
    });

	/*
	// staro za slucaj samo jednog novcica
	L.marker([data_za_novcic.novcic_latitude, data_za_novcic.novcic_longitude], {
		icon: novcicIcon
	}).addTo(mymap);
	*/
}

/*
README UPUTSTVA

u fajlu business-area.blade.php se nalaza sledece

    {znamo_lokaciju: true,
    novcic_latitude: {{$item[0]}},
    novcic_longitude: {{$item[1]}},
    novcic_click_otvori_url: '',
    novcic_popup_slika: 'OVDETREBAURLSLIKE',
    novcic_popup_naslov: ''},

pre nego sto php dodje dotle treba da se iyvrsi provera da li zapravo postoji url slike.
Ukoliko ne postoji, treba da bude ovakav reyultat tj. u nekoj blade {{promenjivu}} bude prayan string

    novcic_popup_slika: ''

ukoliko url do slike postoji, onda naravno krajnji rezultat treba da bude kao na primer

    novcic_popup_slika: '/nekeslike/slike/projekat1slika.jpg'

A jaascript je vec spreman za oba slucaja i kad je prazan string i kad ima putanju do slike.

Sve isto vayi za novcic_click_otvori_url koji treba da bude prazan ukoliko nema linka
    novcic_click_otvori_url: ''


Predlazem za blade
$popupslikaurl = ''
$popupnaslov = ''
$clickurl = ''


    {znamo_lokaciju: true,
    novcic_latitude: {{$item[0]}},
    novcic_longitude: {{$item[1]}},
    novcic_click_otvori_url: {{$popupslikaurl}},
    novcic_popup_slika: {{$popupnaslov}},
    novcic_popup_naslov: {{$clickurl}}},

*/


/*
        window.vise_novcica = [
            @foreach($tackeNaMapi as $item)

                {znamo_lokaciju: true,
                novcic_latitude: {{$item['geoUri'][0]}},
                novcic_longitude: {{$item['geoUri'][1]}},
                novcic_click_otvori_url: {{$item['popupLinkUrl']}},
                novcic_popup_slika: {{$item['popupSlikaUrl']}},
                novcic_popup_naslov: {{$item['popupNaslov']}}
                },

                @endforeach
            ];
            console.log("test blade ");
            console.log(window.vise_novcica);


*/