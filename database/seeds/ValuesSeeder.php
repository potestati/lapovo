<?php

use Illuminate\Database\Seeder;

class ValuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        DB::table('values')->insert([
            'minValue' => 1,
            'maxValue' => 50000,
            'slug' => "to-50000-eur",
            'status' => "0",
            // 'category_id' => "1",
        ]);
        //2
        DB::table('values')->insert([
            'minValue' => 50001,
            'maxValue' => 100000,
            'slug' => "from-50001-to-100000-eur",
            'status' => "0",
        ]);
        //3
        DB::table('values')->insert([
            'minValue' => 100001,
            'maxValue' => 500000,
            'slug' => "from-100001-to-500000-eur",
            'status' => "0",
        ]);
        //4
        DB::table('values')->insert([
            'minValue' => 500001,
            'maxValue' => 1000000,
            'slug' => "from-500001-to-1000000-eur",
            'status' => "0",
        ]);
        //5
        DB::table('values')->insert([
            'minValue' => 1000001,
            'maxValue' => 5000000,
            'slug' => "from-1000001-to-5000000-eur",
            'status' => "0",
        ]);
        //6
        DB::table('values')->insert([
            'minValue' => 5000001,
            'maxValue' => 10000000,
            'slug' => "from 5 mil to 10 mil",
            'status' => "0",
        ]);
        //7
        DB::table('values')->insert([
            'minValue' => 10000001,
            'maxValue' => 50000000,
            'slug' => "from-10-mil-to-50-mil",
            'status' => "0",
        ]);
        //8
        DB::table('values')->insert([
            'minValue' => 50000001,
            'maxValue' => 100000000,
            'slug' => "from-50-mil-to-100-mil",
            'status' => "0",
        ]);
        //9
        DB::table('values')->insert([
            'minValue' => 100000001,
            'maxValue' => 500000001,
            'slug' => "from-100-mil-to-500-mil",
            'status' => "0",
        ]);
        //10
        DB::table('values')->insert([
            'minValue' => 500000001,
            'maxValue' => 0,
            'slug' => "over-500-mil",
            'status' => "0",
        ]);
    }
}
