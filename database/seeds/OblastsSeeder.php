<?php

use Illuminate\Database\Seeder;

class OblastsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        DB::table('oblasts')->insert([
            'areaName' => "air industry",
            'poslovnaOblast' => "VAZDUŠNA INDUSTRIJA",
            'slug' => "air-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//2
        DB::table('oblasts')->insert([
            'areaName' => "car industry",
            'poslovnaOblast' => "AUTO INDUSTRIJA",
            'slug' => "car-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//3
        DB::table('oblasts')->insert([
            'areaName' => "business services",
            'poslovnaOblast' => "POSLOVNE OBLASTI",
            'slug' => "business-services",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//4
        DB::table('oblasts')->insert([
            'areaName' => "chemical industry",
            'poslovnaOblast' => "HEMIJSKA INDUSTRIJA",
            'slug' => "chemical-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//5
        DB::table('oblasts')->insert([
            'areaName' => "construction industry",
            'poslovnaOblast' => "GRAĐEVINSKA INDUSTRIJA",
            'slug' => "construction-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//6
        DB::table('oblasts')->insert([
            'areaName' => "electric industry",
            'poslovnaOblast' => "ELEKTRO INDUSTRIJA",
            'slug' => "electric-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//7
        DB::table('oblasts')->insert([
            'areaName' => "agriculture food and severages industry",
            'poslovnaOblast' => "INDUSTRIJA HRANE, PIĆA I AGROKULTURE",
            'slug' => "agriculture-food-and-severages-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//8
        DB::table('oblasts')->insert([
            'areaName' => "clothing industry",
            'poslovnaOblast' => "INDUSTRIJA ODEĆE",
            'slug' => "clothing-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//9
        DB::table('oblasts')->insert([
            'areaName' => "machines and gear",
            'poslovnaOblast' => "MAŠINE I OPREMA",
            'slug' => "machines-and-gear",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//11
        DB::table('oblasts')->insert([
            'areaName' => "metallurgy",
            'poslovnaOblast' => "METALURGIJA",
            'slug' => "metallurgy",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//11
        DB::table('oblasts')->insert([
            'areaName' => "packaging industry",
            'poslovnaOblast' => "INDUSTRIJA AMBALAŽE",
            'slug' => "packaging-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//12
        DB::table('oblasts')->insert([
            'areaName' => "paper industry",
            'poslovnaOblast' => "INDUSTRIJA PAPIRA",
            'slug' => "paper-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
        //13
        DB::table('oblasts')->insert([
            'areaName' => "pharmaceutical industry",
            'poslovnaOblast' => "FARMACEUTSKA INDUSTRIJA",
            'slug' => "pharmaceutical-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//14
        DB::table('oblasts')->insert([
            'areaName' => "leather industry",
            'poslovnaOblast' => "INDUSTRIJA KOŽE",
            'slug' => "leather-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//15
        DB::table('oblasts')->insert([
            'areaName' => "textile industry",
            'poslovnaOblast' => "INDUSTRIJA TEKSTILA",
            'slug' => "textile-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//16
        DB::table('oblasts')->insert([
            'areaName' => "plastic and rubber",
            'poslovnaOblast' => "PLASTIKA I GUMA",
            'slug' => "plastic-and-rubber",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//17
        DB::table('oblasts')->insert([
            'areaName' => "wood industry",
            'poslovnaOblast' => "DREVNA INDUSTRIJA",
            'slug' => "wood-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//18
        DB::table('oblasts')->insert([
            'areaName' => "software and it technologies",
            'poslovnaOblast' => "SOFTVER I TEHNOLOGIJE",
            'slug' => "software-and-it-technologies",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//19
        DB::table('oblasts')->insert([
            'areaName' => "turistic services",
            'poslovnaOblast' => "TURISTIČKE USLUGE",
            'slug' => "turistic-services",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//20
        DB::table('oblasts')->insert([
            'areaName' => "sports and recreation",
            'poslovnaOblast' => "SPORT I REKREACIJA",
            'slug' => "sports-and-recreation",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//21
        DB::table('oblasts')->insert([
            'areaName' => "loan business",
            'poslovnaOblast' => "KREDITNI POSLOVI",
            'slug' => "loan-business",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//22
        DB::table('oblasts')->insert([
            'areaName' => "free industial zones",
            'poslovnaOblast' => "SLOBODNA INDUSTRIJSKA ZONA",
            'slug' => "free-industial-zones",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//23
        DB::table('oblasts')->insert([
            'areaName' => "energy industry",
            'poslovnaOblast' => "ENERGETSKA INDUSTRIJA",
            'slug' => "energy-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//24
        DB::table('oblasts')->insert([
            'areaName' => "infrastructure",
            'poslovnaOblast' => "INFRASTRUKTURA",
            'slug' => "infrastructure",
            'status' => "0",
            // 'category_id' => "0",
        ]);
        //25
        DB::table('oblasts')->insert([
            'areaName' => "wather industry",
            'poslovnaOblast' => "INDUSTRIJA VODE",
            'slug' => "wather-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//26
        DB::table('oblasts')->insert([
            'areaName' => "eco-industry",
            'poslovnaOblast' => "EKO INDUSTRIJA",
            'slug' => "eco-industry",
            'status' => "0",
            // 'category_id' => "0",
        ]);
//27
        DB::table('oblasts')->insert([
            'areaName' => "non profitable projects the quality of life",
            'poslovnaOblast' => "NEPTROFITABILNI PROJEKTI KVALITET ŽIVOTA",
            'slug' => "non-profitable-projects-the-quality-of-life",
            'status' => "0",
            // 'category_id' => "0",
        ]);

    }
}
