<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvailableProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('available_projects', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('projectName');
            $table->mediumText('projectSector');
            $table->string('slug');
            $table->string('selectedMap');
            $table->mediumText('projectManager');
            $table->mediumText('contactData');
            $table->string('website');
            $table->string('email');
            $table->string('address');
            $table->mediumText('projectDescription');
            $table->mediumText('projectCharacter');
            $table->mediumText('regionCharacter');
            $table->mediumText('offeredCooperation');
            $table->mediumText('certificates');
            $table->mediumText('contactPerson');
            $table->boolean('status');
            $table->string('geo_uri');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')
                ->references('id')->on('categories')->onDelete('cascade');
            $table->integer('oblast_id')->unsigned();
            $table->foreign('oblast_id')
                ->references('id')->on('oblasts')->onDelete('cascade');
            $table->integer('projectValue')->unsigned();
            $table->foreign('projectValue')
                ->references('id')->on('values')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->string('path')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('available_projects');
    }
}
