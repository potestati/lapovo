<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->increments('id');
            $table->string('projectName');
            $table->string('projectSector')->default('Nema podatka');
            $table->string('slug');
            $table->string('selectedMap');
            $table->mediumText('projectManager');
            $table->string('contactData');
            $table->string('website');
            $table->string('email');
            $table->string('address');
            $table->mediumText('projectDescription');
            $table->mediumText('projectCharacter');
            $table->mediumText('regionCharacter');
            $table->mediumText('offeredCooperation');
            $table->mediumText('certificates');
            $table->string('contactPerson');
            $table->boolean('status');
            $table->string('geo_uri')->nullable();
            $table->integer('exact_value');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')
                ->references('id')->on('categories')->onDelete('cascade');
            $table->integer('oblast_id')->unsigned();
            $table->foreign('oblast_id')
                ->references('id')->on('oblasts')->onDelete('cascade');
            $table->integer('projectValue')->unsigned();
            $table->foreign('projectValue')
                ->references('id')->on('values')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->string('path')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
