<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upit extends Model {

    //Table Name
    public $table = "upiti";

    public $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'projectName',
        'projectValue',
        'projectDescription',
        'companyDescr',
        'regionDescr',
        'projectManager',
        'legalRegulations',
        'projectPotencial',
        'contactData',
        'businessArea',
        'scaleValue',
        'project_id'
    ];

    public function project() {
        return $this->belongsTo('App\Project');
    }

}
