<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {
    
    //Table Name
    public $table = "project";
    // Primary Key - ovo sam dodala da li je tako ok ili treba bez ovoga da je id primaryKey
    public $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'projectName',
        'projectSector',
        'slug',
        'projectValue',// scale value id
        'selectedMap',
        'projectManager',
        'contactData',
        'website',
        'email',
        'address',
        'projectDescription',
        'projectCharacter',
        'regionCharacter',
        'offeredCooperation',
        'certificates',
        'contactPerson',
        'user_id',
        'oblast_id',
        'category_id',
        'status',
        'geo_uri',
        'exact_value',
        'path',
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function oblast()
    {
        return $this->hasOne('App\Oblast');
    }

    public function scaleValues()
    {
        return $this->belongsTo('App\ScaleValue');
    }

    public function upit()
    {
        return $this->hasOne('App\Upit');
    }

    public function availableProject()
    {
        return $this->belongsToMany('App\AvailableProjects');
    }

}
