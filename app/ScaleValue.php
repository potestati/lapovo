<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScaleValue extends Model
{
//Table Name
public $table = "values";
// Primary Key
public $primaryKey = 'id';

/**
 * The attributes that are mass assignable.
 *
 * @var array
 */
protected $fillable = [
    'minValue', 
    'maxValue', 
    'slug',
    'status',
    //projekat ce imati koja je kategorija i koja je oblast i koja je skala vrednosti
    // 'category_id',
];


// public function scaleValues()
// {
//     return $this->belongsTo('App\ScaleValue');
// }

public function project(){
    return $this->hasOne('App\Project');
    }
}
