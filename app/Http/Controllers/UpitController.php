<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AvailableProjects;
use App\Project;
use App\Upit;
use DB;

class UpitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function addUpit(Request $request){
		// dd($request->all());
        // return back();

        $this->validator($request);
        
        if($projectValue = $request->projectValue == null){
            $projectValue = 'Nema vrednosti';
        }else{
            $projectValue = $request->projectValue;
        }

        if($projectDescription = $request->projectDescription == null){
            $projectDescription = 'Nema vrednosti';
        }else{
            $projectDescription = $request->projectDescription;
        }

        if($companyDescr = $request->companyDescr == null){
            $companyDescr = 'admin@admin.com';
        }else{
            $companyDescr = $request->companyDescr;
        }

        if($regionDescr = $request->regionDescr == null){
            $regionDescr = 'Nema vrednosti';
        }else{
            $regionDescr = $request->regionDescr;
        }

        if($projectManager = $request->projectManager == null){
            $projectManager = 'Nema vrednosti';
        }else{
            $projectManager = $request->projectManager;
        }

        if($legalRegulations = $request->legalRegulations == null){
            $legalRegulations = 'Nema vrednosti';
        }else{
            $legalRegulations = $request->legalRegulations;
        }

        if($projectPotencial = $request->projectPotencial == null){
            $projectPotencial = 'Nema vrednosti';
        }else{
            $projectPotencial = $request->projectPotencial;
        }

        if($contactData = $request->contactData == null){
            $contactData = 'Nema vrednosti';
        }else{
            $contactData = $request->contactData;
        }        

		Upit::create([
			'projectName'=> $request->projectName, 
			'projectValue'=> $projectValue,
			'projectDescription'=> $projectDescription,
			'companyDescr'=> $companyDescr,
			'regionDescr'=> $regionDescr, 
			'projectManager'=> $projectManager,
			'legalRegulations'=> $legalRegulations,
			'projectPotencial'=> $projectPotencial, 
			'contactData'=> $contactData,
			'businessArea'=> $request->businessArea,
            'scaleValue'=> $request->scaleValue,
            'project_id' => $request->projectId,
		]);


		return redirect('/home')->with('success', 'Vasa registracija je kreirana, uskoro ce Vam se javiti administrator');
    }
    
    protected function validator($request)
    {
        $request->validate([
            'projectName' =>'required',
            'projectValue' => 'required_without_all:projectName',
            'projectDescription' => 'required_without_all:projectName',
            'companyDescr' => 'required_without_all:projectName',
            'regionDescr' => 'required_without_all:projectName',
            'projectManager' => 'required_without_all:projectName',
            'legalRegulations' => 'required_without_all:projectName',
            'projectPotencial' => 'required_without_all:projectName',
            'contactData' => 'required_without_all:projectName',
            'businessArea' => 'required',
            'scaleValue' => 'required',
        ]);
    }



    public function index()
    {
        //$availableProjects = AvailableProjects::all();
        //u view index.blade iz foldera availableProjects , prosledjujemo sve
        //raspolozive projekte iz baze iz tabele availableProjects u varijablu $availableProjects, nju posle pozivamo
        //u view
        //$availableProjects = AvailableProjects::orderBy('title', 'desc')->get();
        //$availableProjects = AvailableProjects::orderBy('title', 'desc')->take(1)->get();
        //get title from only availableProjects two
        //$availableProject = AvailableProjects::where('title', 'AvailableProjects Two')->get();
        //$availableProject = DB::select('SELECT * FROM available_projects');
        $availableProjects = AvailableProjects::orderBy('created_at', 'desc')->paginate(2);

        return view('availableProjects.index')->with('available_projects', $availableProjects);
    }

 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showAvailableProjectsDashboard(){
        return view('availableProjects.adminAvailable');
    }
     //Store funkcija je za validaciju i unos u bazu podataka
    public function create(Request $request)
    {
        //$name=$request->username; 
        //$role = App\Role::find(2);
        //ovo gore je za unos u bazu iz forme iz svakog input polja

        // $this->validate($request, [
        //     'projectName' => 'required',
        //     'status' => 'required',
        //     'category' => 'required',
        //     'amount' => 'required',

        // ]);
        
        //return 123 ovde treba da imamo proveru unosa i poruke kao akciju 
        //nakon submit, pa u folderu inc pravimo novi blade message.blade.php
        
        //Create Available Projects
        $availableProject = new AvailableProjects();
        
        $availableProject->projectName = $request->projectName;
        $availableProject->slug = $request->slug;
        $availableProject->status = $request->status;
        $availableProject->category_id = $request->category_id;
     
        $availableProject->save();

        return redirect('/dashboard')->with('success', 'Available Projects Created');
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $availableProject = AvailableProjects::find($id);
        return view('availableProjects.show')->with('availableProject', $availableProjects);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //edit Available Projects
        $availableProject = AvailableProjects::find($id);
        //NIJE MI JASNO STA SU ARGUMENTI U WITH
        return view('availableProjects.edit')->with('availableProject', $availableProject);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //edit Available Projects
        $this->validate($request, [
            'projectName' => 'required',
            'status' => 'required',
            'category' => 'required',
            'amount' => 'required',

        ]);
        
        //return 123 ovde treba da imamo proveru unosa i poruke kao akciju 
        //nakon submit, pa u folderu inc pravimo novi blade message.blade.php
        
        //Create Available Projects
        $availableProject = AvailableProjects::find($id);
        $availableProject->projectName = $request->input('projectName');
        $availableProject->status = $request->input('status');
        $availableProject->category = $request->input('category');
        $availableProject->amount = $request->input('amount');
        //$availableProject->body = $request->input('body');
        $availableProject->save();

        return redirect('/availableProjects')->with('success', 'Available Projects Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $availableProject = AvailableProjects::find($id);
        $availableProject->delete();
        return redirect('/availableProjects')->with('success', 'Available Projects Removed');

    }
}
