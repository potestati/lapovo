<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fullName' => 'required|string|max:255',
            'companyName' => 'required|string|max:255',
            'mb' => 'required|string|max:255',
            'pib' => 'required|string|max:255',
            'contactPerson' => 'required|string|max:255',
            'website' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'phoneNo' => 'required|string|max:255',
            'additionalData' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'username' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed', 
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // var_dump($data);
        // exit;
        // user
        return User::create([
            'fullName' => $data['fullName'],
            'companyName' => $data['companyName'],
            'mb' => $data['mb'],
            'pib' => $data['pib'],
            'contactPerson' => $data['contactPerson'],
            'website' => $data['website'],
            'address' => $data['address'],
            'phoneNo' => $data['phoneNo'],
            'additionalData' => $data['additionalData'],
            'role_id' => 2,//user id
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
