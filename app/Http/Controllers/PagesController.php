<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\AvailableProjects;
use App\Project;
use App\Upit;
use App\Oblast;
use App\ScaleValue;
use DB;

class PagesController extends Controller
{
    public function index()
    {
        return view('pages.index');
    }
    public function home()
    {
        $categories = Category::all();
        return view('home')->with('categories', $categories);
    }
    public function projectCategories(){
        $categories = Category::all();
        return view('pages.project-categories')->with('categories', $categories);
    }
    public function about()
    {
        $title = 'ПРОФИЛ ФИРМЕ';
        return view('pages.about')->with('title', $title);
    }
    public function services()
    {
        $data =  array(
            'title' => 'Services',
            'services' => ['Web Design', 'Programming', 'SEO']
        );
        return view('pages.services')->with($data);
    }
    public function businessArea($catName){
        $categories = Category::all();
        $category = Category::where('slug', $catName)->first();

        $hasActiveProjectsWithCat = DB::table('project')->where("category_id", $category->id)->where('status', 1)->distinct()->get();

        $collection = collect([]);
        $collectionOfValues = collect([]);
        foreach($hasActiveProjectsWithCat as $activeProject){
            $collection->push(Oblast::where('id', $activeProject->oblast_id)->first()->id);
            $collectionOfValues->push(ScaleValue::where('id', $activeProject->projectValue)->first()->id);
        }

        $oblasts = Oblast::all();
        $scaleValues = ScaleValue::all();

        // $geoUri_temp = AvailableProjects::all();

        $approvedProjects = DB::table('project')->where("category_id", $category->id)->where('status', 1)->distinct()->get();

        // $geoUri = $this->parsirajArr($geoUri_temp);
        $tackeNaMapi = $this->parsirajArr($approvedProjects);
   
        return view('pages.business-area')->with(array(
            'category'=>$category, 
            'collection'=>$collection,
            'categories'=>$categories,
            // 'geoUri'=> $geoUri,
            'tackeNaMapi'=> $tackeNaMapi,
            'oblasts' => $oblasts,
            'scaleValues' => $scaleValues,
            'collectionOfValues' => $collectionOfValues,
        ));
    }
    private function parsirajArr($arr){
        $tackeNaMapi = [];
        foreach($arr as $item){
            //$this predstavlja ovu klasu
            $slikaUrl = '/images/' . $item->path;
            // $otvoriUrl = '/category/{catSlug}/area/{areaSlug}/project/' . 'SLUG';
            $otvoriUrl = '/project/' . $item->slug;
            $novaTacka = array(
                'geoUri' => $this->parsirajSingleLatLong($item->geo_uri),
                'popupSlikaUrl' => $slikaUrl,
                'popupOtvoriUrl' => $otvoriUrl,
                'popupNaslov' => $item->projectName,
                'blabla' => 'blabla',
            );
            $tackeNaMapi[] = $novaTacka;
        }
        return $tackeNaMapi;
    }
    private function parsirajSingleLatLong($geouri){
        $arr_dq = explode ( ':' , $geouri ); // cepa string tamo gde je dvotacka
        $arr_qm = explode ( '?' , $arr_dq[1] ); // cepa string tamo gde je upitnik
        $latlong = explode ( ',' , $arr_qm[0] ); // cepa string tamo gde je yarey
        return $latlong;
    }
    public function displayPhoto($arr){
        $photos = [];
        foreach($arr as $item){
            $photos[] = $item->path;
        }
        return $photos;
    }
    //raspolozivi projekti koji su uneti za potencijalne investitore
    public function availableProjects($catName){
        
        $category = Category::where('slug', $catName)->first();

        $availableProjects = $category->availableProjects;
        $paket['availableProjects'] = $category->availableProjects;
        $paket['drugiPodatak'] = 'drugi podatak';
        
        return view('availableProjects.available')->with('paket', $paket);
    }
    // biranje projekta za konkurs od strane stranog investitora iz oblasti
    public function selectProject($catSlug, $oblastSlug){

        $projectCategory = Category::where('slug','=', $catSlug)->first();
        
        $projectOblast = Oblast::where('slug','=', $oblastSlug)->first();
        
        $projects = DB::table('project')->where("category_id", $projectCategory->id)->where('oblast_id', $projectOblast->id)->get();

        $scaleOfValues = ScaleValue::all();
        $collection = collect([]);

        foreach($projects as $project){
            $collection->push(ScaleValue::where('id', $project->projectValue)->first()->id);
        }

        $geoUri_temp = Project::all();

        $selectedGeoUri = DB::table('project')->where("category_id", $projectCategory->id)->where('oblast_id', $projectOblast->id)->get();

        $tackeNaMapi = $this->parsirajArr($selectedGeoUri);
        // $geoUri = $this->parsirajArr($geoUri_temp);

        $categories = Category::all();
    
        return view('availableProjects.available')->with(array(
            'projectCategory'=>$projectCategory, 
            'projectOblast'=>$projectOblast,
            'projects'=>$projects,
            // 'geoUri'=>$geoUri,
            'tackeNaMapi'=> $tackeNaMapi,
            'categories'=> $categories,
            'collection'=> $collection,
            'scaleOfValues'=> $scaleOfValues,
        ));
    }
    // biranje projekta za konkurs od strane stranog investitora iz skale vrednosti
    public function selectProjectbyValues($catSlug, $valueSlug){

        $projectCategory = Category::where('slug','=', $catSlug)->first();
        $projectValue = ScaleValue::where('slug','=', $valueSlug)->first();

        $projects = DB::table('project')->where("category_id", $projectCategory->id)->where('projectValue', $projectValue->id)->get();

        $projectOblasts = Oblast::all();
        $collection = collect([]);

        foreach($projects as $project){
            $collection->push(Oblast::where('id', $project->oblast_id)->first()->id);
        }

        // $geoUri_temp = AvailableProjects::all();
        $selsectedGeoUri = DB::table('project')->where("category_id", $projectCategory->id)->where('projectValue', $projectValue->id)->get();

        // $geoUri = $this->parsirajArr($selsectedGeoUri);
        $tackeNaMapi = $this->parsirajArr($selsectedGeoUri);

        $categories = Category::all();

        return view('availableProjects.index')->with(array(
            'projectCategory'=>$projectCategory, 
            'projectValue'=>$projectValue,
            'projects'=>$projects,
            // 'geoUri'=>$geoUri,
            'tackeNaMapi'=> $tackeNaMapi,
            'categories'=> $categories,
            'collection'=> $collection,
            'projectOblasts'=> $projectOblasts,
            'projectCategory'=> $projectCategory,
        ));
    }

    public function projectListByAreaAndValue($catSlug, $oblastSlug, $valueSlug){

        $projectCategory = Category::where('slug','=', $catSlug)->first();
        // dd($projectCategory->id);
        $projectOblast = Oblast::where('slug','=', $oblastSlug)->first();
        // dd($projectOblast->id);
        $projectValue = ScaleValue::where('slug', $valueSlug)->first();
        // dd($projectValue->id);
        $projects = DB::table('project')->where("category_id", $projectCategory->id)->where('oblast_id', $projectOblast->id)->where('projectValue', $projectValue->id)->get();
        
        // $oblasts = Oblast::all();
        // $collection = collect([]);

        // foreach($projects as $project){
        //     $collection->push(Oblast::where('id', $project->oblast_id)->first()->id);
        // }

        $selectedGeoUri = DB::table('project')->where("category_id", $projectCategory->id)->where('oblast_id', $projectOblast->id)->where('projectValue', $projectValue->id)->get();

        // $geoUri_temp = AvailableProjects::all();
        $tackeNaMapi = $this->parsirajArr($selectedGeoUri);
        // $geoUri = $this->parsirajArr($geoUri_temp);

        $categories = Category::all();
    
        return view('availableProjects.project-list-by-area-and-value')->with(array(
            'projectCategory'=>$projectCategory, 
            'projectOblast'=>$projectOblast,
            'projectValue'=>$projectValue,
            'projects'=>$projects,
            'tackeNaMapi'=>$tackeNaMapi,
            'categories'=> $categories,
            // 'collection'=> $collection,
            // 'oblasts'=> $oblasts,
        ));
    }

    public function showSingleProject($catSlug, $areaSlug, $projectSlug){
        $selectedCategory = Category::where('slug', $catSlug)->first();
        $categories = Category::all();
        $selectedArea = Oblast::where('slug', $areaSlug)->first();
        $selectedProject = Project::where('slug', $projectSlug)->first();
        
        //iz modela Project pozivam usera preko funkcije user iz modela
        $userData = $selectedProject->user;
        // $userData = $userData->toArray();

        // $geoUri_temp = AvailableProjects::all();
        $selectedGeoUri = DB::table('project')->where("category_id", $selectedCategory->id)->where('oblast_id',$selectedArea->id)->where('id', $selectedProject->id)->get();

        $tackeNaMapi = $this->parsirajArr($selectedGeoUri);
       
        return view('availableProjects.show')->with(array(
            'selectedCategory'=> $selectedCategory,
            'categories'=> $categories,
            'selectedArea'=> $selectedArea,
            'selectedProject'=> $selectedProject,
            'userData'=> $userData,
            'tackeNaMapi'=>$tackeNaMapi,
        ));
    }

    public function showSingleProjectPojednostavljena($projectSlug){

        $categories = Category::all();
        $selectedProject = Project::where('slug', $projectSlug)->first();

        // $projectCategory = Category::where('id', $selectedProject->category_id)->first();
        // dd($projectCategory);
        // $selectedGeoUri = DB::table('available_projects')->where("category_id", $selectedCategory->id)->where('oblast_id',$selectedArea->id)->where('id', $selectedProject->id)->get();

        $selectedCategory = Category::where('id', $selectedProject->category_id)->first();
        $selectedArea = Oblast::where('id', $selectedProject->oblast_id)->first();
        
        //iz modela Project pozivam usera preko funkcije user iz modela
        $userData = $selectedProject->user;
        // $userData = $userData->toArray();

        // $geoUri_temp = AvailableProjects::all();
        $selectedGeoUri = DB::table('project')->where('id', $selectedProject->id)->get();

        $tackeNaMapi = $this->parsirajArr($selectedGeoUri);
       
        return view('availableProjects.single-project')->with(array(
            'selectedCategory'=> $selectedCategory,
            'selectedArea'=> $selectedArea,
            'selectedProject'=> $selectedProject,
            'userData'=> $userData,
            'tackeNaMapi'=>$tackeNaMapi,
            'categories'=>$categories,
        ));
    }

    //ova funkcija pokazuje odabrani projekat nakon selektovane vrednosti
    public function showSingleProjectByValues($catSlug, $valueSlug, $projectSlug){
        $selectedCategory = Category::where('slug', $catSlug)->first();
        $categories = Category::all();
        $selectedValue = ScaleValue::where('slug', $valueSlug)->first();
        $selectedProject = Project::where('slug', $projectSlug)->first();
        //iz modela Project pozivam usera preko funkcije user iz modela
        $userData = $selectedProject->user;

        $geoUri_temp = Project::where('id', $selectedProject->id)->first();
        $selectedGeoUri = DB::table('project')->where("category_id", $selectedCategory->id)->where('projectValue', $selectedValue->id)->where('id', $selectedProject->id)->get();

        $tackeNaMapi = $this->parsirajArr($selectedGeoUri);
       
        return view('availableProjects.show-by-value')->with(array(
            'selectedCategory'=> $selectedCategory,
            'categories'=> $categories,
            'selectedValue'=> $selectedValue,
            'selectedProject'=> $selectedProject,
            'userData'=> $userData,
            'tackeNaMapi'=>$tackeNaMapi,
        ));
    }

    // ova funkcija radi unos projekta od strane ponudjaca projekta
    public function project($catSlug){
        $projectCategory = Category::where('slug','=', $catSlug)->first();
        // $oblasts = Oblast::where('slug','=', $oblastSlug)->first();
        $oblasts = Oblast::all();

        return view('projects.project')->with('projectCategory', $projectCategory)->with('oblasts', $oblasts);
    }

    public function showUpit($projectName, $projectId){
        $oblasts = Oblast::all();
        $values = ScaleValue::all();
        $upit = Upit::where('projectName','=', $projectName)->first();
        return view('upiti.upit')->with('projectId', $projectId)->with('oblasts', $oblasts)->with('values', $values);
    }

        /* set language */
        public function set($lang) {
            // dd(\App::getLocale());
            session(['applocale' => $lang]);
    
            return back();
        }
}
