<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Project;
use App\Category;
use App\Upit;
use App\AvailableProjects;
use Auth;
use App\User;
use App\Role;
use DB; 
use App\Oblast;
use PDF;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

     //svaki metod koji prodje kroz DashboardController ce biti kontrolisan od strane middleware
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if($user->isAdmin()){
            //ovde dobijam project tabelu i samo dodajem podatke iz user i category tabele
            $projects = $this->get_customer_data();
            // $projects = Project::with('user')->with('category')->get();
            return view('dashboard')->with('projects', $projects);
        }else{
            return redirect('/');
        }
    }

    /* set language */
    public function set($lang) {
        // dd(\App::getLocale());
        session(['applocale' => $lang]);

        return back();
    }


    public function get_customer_data(){
        $customer_data = Project::with('user')->with('category')->get();
        return $customer_data;
    }

    public function pdf(){
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($this->convert_customer_data_to_html());
        return $pdf->stream();
        // dd($pdf);
    }

    public function convert_customer_data_to_html(){
        $customer_data = $this->get_customer_data();
        $output = '';
        $output .= '<table width="100%" style="border-collapse: collapse; border: 0px;">
        <thead class="thead-light">
            <tr>
                <th style="border: 1px solid; padding:12px;" width="20%">ID</th>
                <th style="border: 1px solid; padding:12px;" width="20%">Ime i prezime</th>
                <th style="border: 1px solid; padding:12px;" width="20%">Kategorija</th>
                <th style="border: 1px solid; padding:12px;" width="20%">Naziv projekta</th>
                <th style="border: 1px solid; padding:12px;" width="20%">status</th>
            </tr>
        </thead>';
        foreach($customer_data as $index => $costumer){
            $projectOwnerName = $costumer->user->fullName;
            $projectCat = $costumer->category->imeKategorije;
            if($costumer->status == 0){
                $status = 'U procesu';
            }elseif($costumer->status == 1){
                $status = 'Odobren';
            }elseif($costumer->status == 2){
                $status = 'Odbijen';
            }elseif($costumer->status == 3){
                $status = 'Realizovan';
            }
            // dd($projectCat);
            $output .='
            <tr>
            <th style="border: 1px solid; padding:12px;" width="20%">' . ++$index . '</th>
            <td style="border: 1px solid; padding:12px;">' . $projectOwnerName . '</td>
            <td style="border: 1px solid; padding:12px;">' . $projectCat . '</td>
            <td style="border: 1px solid; padding:12px;">' . $costumer->projectName . '</td>
            <td style="border: 1px solid; padding:12px;">' . $status . '</td>
            </tr>';
        }
        $output .= '<table>';
        return $output;
    }

    public function singleApplication($id){

        $user = Auth::user();

        if($user->isAdmin()){
            $projects = Project::with('user')->with('category')->get();
            return view('projectReview')->with('singleApplication', $projects);
        }else{
            return redirect('/');
        }

    }

    public function showprojectReview($id){
        $user = Auth::user();
        if($user->isAdmin()){
            $project = Project::findOrFail($id);
            $upit = DB::table('upiti')->where('project_id',$project->id)->get();
            $oblasts = Oblast::all();
            $scaleValue = $upit[0]->scaleValue;
            // dd($upit);
            return view('projectReview')->with('scaleValue', $scaleValue)->with('singleProject',  $project)->with('upit', $upit)->with('oblasts', $oblasts);
        }else{
            return redirect('/');
        }
    }

    public function addStatus(Request $request){
        $projectsAll = $request->all();
        // echo "<pre>";
          foreach($projectsAll as $key => $status){
            if($key != '_token'){
                $projectId = explode("_", $key )[1];
                //var_dump($projectStatus);
                DB::table('project')->where('id', $projectId)->update(['status'=> $status]);
            }
        }
        // echo "</pre>";
        // $project_id = $request->id;
        // $status = $request->status;
        // DB::table('project')->where('id', $project_id)->update(['status'=> $status]);

        return redirect()->back();
    }

    public function projectList(){

        $user = Auth::user();

        if($user->isAdmin()){
            // $projectList = AvailableProjects::all();
            // $categories = DB::table('categories')->where('id',$projectList->id)->get();
            // $oblasts = DB::table('oblasts')->where('id',$projectList->oblast_id)->get();
            // $projectValue = DB::table('values')->where('id',$projectList->projectValue)->get();

            $projects = DB::table('project')->get();
            foreach($projects as $project){
                // foreach($projectList as $availableProject){
                //     if($project->id == $availableProject->id){
                //         $approvedProjects[] = $project;
                //     }
                // }
                $approvedProjects[] = $project;
            }
            // dd($approvedProjects);
            return view('project-list')->with('approvedProjects',  $approvedProjects);
        }else{
            return redirect('/');
        }
    }

    public function projectActivation()
    {
        $user = Auth::user();

        if($user->isAdmin()){
            $user = User::all();
            $categories = Category::all();
            $oblasts = Oblast::all();
            
            $projects = Project::with('user', $user)->with('category', $categories)->with('oblasts', $oblasts)->get();
            return view('project-activation')->with('projects', $projects);
        }else{
            return redirect('/');
        }
    }
    
}
