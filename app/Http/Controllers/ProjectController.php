<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Category;
use App\Oblast;
use App\Upit;
use App\ScaleValue;

class ProjectController extends Controller {

    public function addProject(Request $request) {

        $this->validator($request);

        if($file = $request->file('file')){
            $name = $file->getClientOriginalName();
            $file->move('images', $name);
            $input['path'] = $name;
        }else{
            $name = str_random(25);
        }
        
        $value_id = ScaleValue::where('minValue','<=', $request->projectValue)->orderBy('id', 'DESC')->first()->id;

        $slug = str_slug($request->projectName, '-');

        // $data = $request->all();
        // dd($data);

        // foreach($data as $key=>$value){
        //     if($value == null){
        //         $value = 'Nema vrednosti';
        //         // $project = Project::create([
        //         //     $key => $value,
        //         // ]);
        //         dd($key);
        //     }else{
        //         $project = Project::create([
        //             $key => $value,
        //         ]);
        //     }
        // }

        if($projectSector = $request->projectSector == null){
            $projectSector = 'Nema vrednosti';
        }else{
            $projectSector = $request->projectSector;
        }

        if($selectedMap = $request->selectedMap == null){
            $selectedMap = 'Nema vrednosti';
        }else{
            $selectedMap = $request->selectedMap;
        }

        if($projectManager = $request->projectManager == null){
            $projectManager = 'Nema vrednosti';
        }else{
            $projectManager = $request->projectManager;
        }

        if($contactData = $request->contactData == null){
            $contactData = 'Nema vrednosti';
        }else{
            $contactData = $request->contactData;
        }

        if($website = $request->website == null){
            $website = 'Nema vrednosti';
        }else{
            $website = $request->website;
        }

        if($email = $request->email == null){
            $email = 'admin@admin.com';
        }else{
            $email = $request->email;
        }

        if($address = $request->address == null){
            $address = 'Nema vrednosti';
        }else{
            $address = $request->address;
        }

        if($projectDescription = $request->projectDescription == null){
            $projectDescription = 'Nema vrednosti';
        }else{
            $projectDescription = $request->projectDescription;
        }

        if($projectCharacter = $request->projectCharacter == null){
            $projectCharacter = 'Nema vrednosti';
        }else{
            $projectCharacter = $request->projectCharacter;
        }

        if($regionCharacter = $request->regionCharacter == null){
            $regionCharacter = 'Nema vrednosti';
        }else{
            $regionCharacter = $request->regionCharacter;
        }

        if($offeredCooperation = $request->offeredCooperation == null){
            $offeredCooperation = 'Nema vrednosti';
        }else{
            $offeredCooperation = $request->offeredCooperation;
        }        
        
        if($certificates = $request->certificates == null){
            $certificates = 'Nema vrednosti';
        }else{
            $certificates = $request->certificates;
        }

        if($contactPerson = $request->contactPerson == null){
            $contactPerson = 'Nema vrednosti';
        }else{
            $contactPerson = $request->contactPerson;
        }

        $project = Project::create([
            'projectName' => $request->projectName,
            'projectSector' => $projectSector,
            'slug' => $slug,
            'exact_value' => $request->projectValue,  
            'projectValue' => $value_id,
            'selectedMap' => $selectedMap,
            'projectManager' => $projectManager,
            'contactData' => $contactData,
            'website' => $website,
            'email' => $email,
            'address' => $address,
            'projectDescription' => $projectDescription,
            'projectCharacter' => $projectCharacter,
            'regionCharacter' => $regionCharacter,
            'offeredCooperation' => $offeredCooperation,
            'certificates' => $certificates,
            'contactPerson' => $contactPerson,
            'user_id' => Auth::user()->id,
            'oblast_id' => $request->oblast_id,
            'category_id' => $request->prosledjeniCategoryID,
            'status' => 0,
            'path'=> $name,
        ]);

        return redirect()->route('upit', [
            'projectName' => str_replace(' ', '-', $request->projectName),
            'projectId' => $project->id
            ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator($request)
    {
        $request->validate([
            'projectName' =>'required',
            'projectSector' => 'required_without_all:projectName',
            'projectValue' => 'required',
            'selectedMap' => 'required_without_all:projectName',
            'projectManager' => 'required_without_all:projectName',
            'contactData' => 'required_without_all:projectName',
            'website' => 'required_without_all:projectName',
            'email' => 'required_without_all:projectName',
            'address' => 'required_without_all:projectName',
            'projectDescription' => 'required_without_all:projectName',
            'projectCharacter' => 'required_without_all:projectName',
            'regionCharacter' => 'required_without_all:projectName',
            'offeredCooperation' => 'required_without_all:projectName',
            'certificates' => 'required_without_all:projectName',
            'contactPerson' => 'required_without_all:projectName',
            'oblast_id' => 'required',
        ]);
    }


    public function updateProject(Request $request, $id){
        $project = Project::findOrFail($id);
        $project->update($request->all());
        $upit1 = $project->upit()->first();

        //id scale of values
        $value_id = ScaleValue::where('minValue','<=', $request->exact_value)->orderBy('id', 'DESC')->first()->id;
        //oblast
        if($request->businessArea == null){
            $oblast_id = Oblast::where('poslovnaOblast', $upit1->businessArea)->first()->id;
        }else{
            $oblast_id = Oblast::where('poslovnaOblast', $request->businessArea)->first()->id;
        }
        //slug
        $slug = str_slug($request->projectName, '-');

        $project->update(array(
            'projectValue' => $value_id,
            'oblast_id' => $oblast_id,
            'slug' => $slug,
        ));
        
        //ovaj deo se odnosi na update upit tabele a ovde su izuzeta sva polja koja se nalaze u project tabeli
        //a ne nalaze se u upit tabeli kako ne bi doslo do greske
        $upit = $project->upit()->update($request->except([
            'category_id', 
            '_token',
            'projectSector',
            'selectedMap',
            'website',
            'email',
            'address',
            'projectCharacter',
            'regionCharacter',
            'offeredCooperation',
            'certificates',
            'contactPerson',
            'user_id',
            'oblast_id',
            'category_id',
            'exact_value',
            'status',
            '#',
            ]));
            //ova polja se nisu updatovala pa sam to resila ovako
            $upit = $project->upit()->update(array(
                'companyDescr' => $request->companyDescr,
                'regionDescr' => $request->regionDescr,
                'legalRegulations' => $request->legalRegulations,
                'projectPotencial' => $request->projectPotencial,
                'projectValue' => $request->exact_value,
            ));

        return redirect('/dashboard');
    }
}
