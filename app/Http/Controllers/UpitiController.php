<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Upit;
use DB;

class UpitiController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //$upiti = Upit::all();
        //u view index.blade iz foldera upiti , prosledjujemo sve
        //upite iz baze iz tabele upiti u varijablu $upiti, nju posle pozivamo
        //u view
        //$upiti = Upit::orderBy('title', 'desc')->get();
        //$upiti = Upit::orderBy('title', 'desc')->take(1)->get();
        //get title from only upit two
        //$upit = Upit::where('title', 'Upit Two')->get();
        //$upit = DB::select('SELECT * FROM upiti');
        $upiti = Upit::orderBy('created_at', 'desc')->paginate(2);

//da li with('posts', $posts); argument posts/ je ustvari tabela iz baze
        return view('upiti.index')->with('upiti', $upiti);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('upiti.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //Store funkcija je za validaciju i unos u bazu podataka
    public function store(Request $request) {
        //$name=$request->username; 
        //$role = App\Role::find(2);
        //ovo gore je za unos u bazu iz forme iz svakog input polja

        $this->validate($request, [

            'projectName'=> 'required',
            'projectValue'=> 'required',
            'projectDescription'=> 'required',
            'companyDescr'=> 'required',
            'regionDescr'=> 'required', 
            'projectManager'=> 'required', 
            'legalRegulations'=> 'required',
            'projectPotencial'=> 'required',
            'contactData'=> 'required',
            'businessArea'=> 'required', 
            'scaleValue'=> 'required',
            'category' => 'required',
        ]);

        //return 123 ovde treba da imamo proveru unosa i poruke kao akciju 
        //nakon submit, pa u folderu inc pravimo novi blade message.blade.php
        //Create Upit
        $upit = new Upit;
        $upit->projectName = $request->input('projectName');
        $upit->projectValue = $request->input('projectValue');
        $upit->projectDescription = $request->input('projectDescription');
        $upit->companyDescr = $request->input('companyDescr');
        $upit->regionDescr = $request->input('regionDescr');
        $upit->projectManager = $request->input('projectManager');
        $upit->legalRegulations = $request->input('legalRegulations');
        $upit->projectPotencial = $request->input('projectPotencial');
        $upit->contactData = $request->input('contactData');
        $upit->businessArea = $request->input('businessArea');
        $upit->scaleValue = $request->input('scaleValue');
        $upit->category = $request->input('category');

        //$upit->body = $request->input('body');
        $upit->save();

        return redirect('/upiti')->with('success', 'Upit Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $upit = Upit::find($id);
        return view('upiti.show')->with('upiti', $upit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //edit upit
        $upit = Upit::find($id);
        return view('upiti.edit')->with('upit', $upit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //edit upit
        $this->validate($request, [

            'projectName' => 'required', 
            'projectValue' => 'required',
            'projectDescription' => 'required', 
            'companyDescr' => 'required',
            'regionDescr' => 'required', 
            'projectManager' => 'required',
            'legalRegulations' => 'required', 
            'projectPotencial' => 'required', 
            'contactData' => 'required',
            'businessArea' => 'required',
            'scaleValue' => 'required',
            'category' => 'required',
        ]);

        //return 123 ovde treba da imamo proveru unosa i poruke kao akciju 
        //nakon submit, pa u folderu inc pravimo novi blade message.blade.php
        //Create Upit

        $upit = Upit::find($id);
        $upit->projectName = $request->input('projectName');
        $upit->projectValue = $request->input('projectValue');
        $upit->projectDescription = $request->input('projectDescription');
        $upit->companyDescr = $request->input('companyDescr');
        $upit->regionDescr = $request->input('regionDescr');
        $upit->projectManager = $request->input('projectManager');
        $upit->legalRegulations = $request->input('legalRegulations');
        $upit->projectPotencial = $request->input('projectPotencial');
        $upit->contactData = $request->input('contactData');
        $upit->businessArea = $request->input('businessArea');
        $upit->scaleValue = $request->input('scaleValue');
        $upit->category = $request->input('category');
        //$upit->body = $request->input('body');
        $upit->save();

        return redirect('/upiti')->with('success', 'Upit Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $upit = Upit::find($id);
        $upit->delete();
        return redirect('/upiti')->with('success', 'Upit Removed');
    }

}
