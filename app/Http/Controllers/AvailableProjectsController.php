<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AvailableProjects;
use App\Project;
use App\Category;
use App\Oblast;
use DB;

class AvailableProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$availableProjects = AvailableProjects::all();
        //u view index.blade iz foldera availableProjects , prosledjujemo sve
        //raspolozive projekte iz baze iz tabele availableProjects u varijablu $availableProjects, 
        //nju posle pozivamo
        //u view
        //$availableProjects = AvailableProjects::orderBy('title', 'desc')->get();
        //$availableProjects = AvailableProjects::orderBy('title', 'desc')->take(1)->get();
        //get title from only availableProjects two
        //$availableProject = AvailableProjects::where('title', 'AvailableProjects Two')->get();
        //$availableProject = DB::select('SELECT * FROM available_projects');
        $availableProjects = AvailableProjects::orderBy('created_at', 'desc')->paginate(2);

        return view('availableProjects.index')->with('available_projects', $availableProjects);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showAvailableProjectsDashboard(){

        $approvedProjects = Project::where('status', 1)->get();
        // dd($approvedProjects);
        return view('availableProjects.adminAvailable')->with('approvedProjects', $approvedProjects);
    }
     //Store funkcija je za validaciju i unos u bazu podataka
     //treba srediti migraciju da available project ima oblast_id koji je referenca na id iz tabele oblasts
    public function create(Request $request){
        $projectName = $request->project;
        // dd($projectName);
        $activeProjects = Project::where('id', $projectName)->get();
        // dd($activeProjects[0]["id"]);
        $activeProjects = $activeProjects[0]["id"];
        // dd($activeProjects->projectSector);
        $geo_uri = $request->geo_uri;

        // $projectName = $activeProjects->projectName;
        // $projectSector = $activeProjects->projectSector;
        // $slug = $activeProjects->slug;
        // $projectValue = $activeProjects->projectValue;
        // $selectedMap = $activeProjects->selectedMap;
        // $projectManager = $activeProjects->projectManager;
        // $contactData = $activeProjects->contactData;
        // $website = $activeProjects->website;
        // $email = $activeProjects->email;
        // $address = $activeProjects->address;
        // $projectDescription = $activeProjects->projectDescription;
        // $projectCharacter = $activeProjects->projectCharacter;
        // $regionCharacter = $activeProjects->regionCharacter;
        // $offeredCooperation = $activeProjects->offeredCooperation;
        // $certificates = $activeProjects->certificates;
        // $contactPerson = $activeProjects->contactPerson;
        // $user_id = $activeProjects->user_id;
        // $oblast_id = $activeProjects->oblast_id;
        // $category_id = $activeProjects->category_id;
        // $status = $activeProjects->status;
        // $path = $activeProjects->path;

        // dd($geo_uri);

        $project = Project::whereId($activeProjects)->first();
        // dd($project);
        $project->update(array(
            'geo_uri' => $geo_uri,
        ));

        // AvailableProjects::create([
        //     'projectName' => $projectName,
        //     'projectSector' => $projectSector,
        //     'slug' => $slug,
        //     'projectValue' => $projectValue,
        //     'selectedMap' => $selectedMap,
        //     'projectManager' => $projectManager,
        //     'contactData' => $contactData,
        //     'website' => $website,
        //     'email' => $email,
        //     'address' => $address,
        //     'projectDescription' => $projectDescription,
        //     'projectCharacter' => $projectCharacter,
        //     'regionCharacter' => $regionCharacter,
        //     'offeredCooperation' => $offeredCooperation,
        //     'certificates' => $certificates,
        //     'contactPerson' => $contactPerson,
        //     'user_id' => $user_id,
        //     'oblast_id' => $oblast_id,
        //     'category_id' => $category_id,
        //     'status' => $status,
        //     'geo_uri' => $geo_uri,
        //     'path'=> $path,
        // ]);

        return redirect('/dashboard');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $availableProject = AvailableProjects::find($id);
        return view('availableProjects.show')->with('availableProject', $availableProjects);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //edit Available Projects
        $availableProject = AvailableProjects::find($id);
        return view('availableProjects.edit')->with('availableProject', $availableProject);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //edit Available Projects
        $this->validate($request, [
            'projectName' => 'required',
            'status' => 'required',
            'category' => 'required',
            'amount' => 'required',

        ]);
        
        //return 123 ovde treba da imamo proveru unosa i poruke kao akciju 
        //nakon submit, pa u folderu inc pravimo novi blade message.blade.php
        
        //Create Available Projects
        $availableProject = AvailableProjects::find($id);
        $availableProject->projectName = $request->input('projectName');
        $availableProject->status = $request->input('status');
        $availableProject->category = $request->input('category');
        $availableProject->amount = $request->input('amount');
        //$availableProject->body = $request->input('body');
        $availableProject->save();

        return redirect('/availableProjects')->with('success', 'Available Projects Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $availableProject = AvailableProjects::find($id);
        $availableProject->delete();
        return redirect('/availableProjects')->with('success', 'Available Projects Removed');

    }
}
