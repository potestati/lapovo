<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'categoryName', 
        'categoryDescription', 
        'categoryImage', 
        'slug',
    ];

    public function availableProjects()
    {
        // resano sa db one to one , meny to one, one to meny and meny to meny
        //ovde pozivamo model AvailableProjects u model Category jer ako se klikne na odredjenu kategoriju otvaraju se projekti koji su vezani 
        //za tu kategoriju , relacija u db
        //pa onda pozivamo tu funkciju ovu tacnije funkciju availableProjects() u controller
        return $this->hasMany('App\AvailableProjects');
    }

    // public function category() {
    //     return $this->belongsTo('App\Category');
    // }

    public function projects()
    {
        return $this->hasMany('App\Project', 'category_id');
    }

    // public function oblasts()
    // {
    //     return $this->hasMany('App\Oblast');
    // }
}
