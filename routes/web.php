<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/hello', function () {
    /*    return view('welcome');*/
    return ' Hello ';
});
Route::get('/about', function () {
    return view('pages.about');
});
/*  http://laravelapp.local/users/Brad u url
output je This is user Brad*/
/*Route::get('/users/{id}', function ($id) {
    return 'This is user ' . $id;
});*/
/* http://laravelapp.local/users/2/Brad ovo je u url adresi za pozivanje stranice za ovu dole rutu*/
Route::get('/users/{id}/{name}', function ($id, $name) {
    return 'This is user ' . $name . 'with an id of '. $id;
});

Route::middleware('languageSwitcher')->group(function () {

/* start */
Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/services', 'PagesController@services');
/** putanja za ponudjaca projekta 
 * -------------------------------------
*/
Route::get('/home', 'PagesController@home')->name('homeStart');
//unos projekta prva forma
Route::get('/selected-category/{catName}', 'PagesController@project')->name('selectedCategory');
//unos projekta prva forma ovde sacuva podatke iz requesta
Route::post('/project/add', 'ProjectController@addProject')->name('addProject');
//ovde se radi redirekcija sa prve forme za unos projekta na upit formu, a podaci su poslati u db
Route::get('/availableProjects/{projectName}/upit/{projectId}', 'PagesController@showUpit')->name('upit');
Route::post('/upit/add', 'UpitController@addUpit')->name('addUpit'); 

/* putanja za investora 
*------------------------------
*/
Route::get('/project-categories', 'PagesController@projectCategories')->name('projectCategories');
Route::get('/category/{catName}', 'PagesController@businessArea');
//selekcija po oblastima
Route::get('/category/{catName}/business-area/{oblastName}', 'PagesController@selectProject');
//selekcija po vrednosti
Route::get('/category/{catName}/scale-of-values/{valueSlug}', 'PagesController@selectProjectbyValues');
//project list by value after selected area
Route::get('/category/{catSlug}/area/{areaSlug}/value/{valueSlug}', 'PagesController@projectListByAreaAndValue');
//ruta za single project
Route::get('/category/{catSlug}/area/{areaSlug}/project/{projectSlug}', 'PagesController@showSingleProject')->name('showSingleProject'); //
Route::get('/category/{catSlug}/value/{valueSlug}/project/{projectSlug}', 'PagesController@showSingleProjectByValues');

//Route::get('/category/{catName}', 'PagesController@availableProjects');
// Route::get('/availableProjects/{projectSlug}', 'PagesController@project');
Route::post('/project/{id}', 'ProjectController@updateProject')->name('updateProject');
//kreiran sa naredbom $ php artisan make:controller PostController --resource
Route::resource('posts', 'PostsController');
Route::resource('projects', 'ProjectsController');

//Single project from coin
Route::get('/project/{projectSlug}', 'PagesController@showSingleProjectPojednostavljena')->name('projectSlug'); 

// Dashboard
Route::get('/dashboard/availableProjects', 'AvailableProjectsController@showAvailableProjectsDashboard')->name('showAvailableProjectsDashboard');
Route::post('/dashboard/availableProjects', 'AvailableProjectsController@create')->name('createAvailableProjects');
// Admin / Dashboard panel
Auth::routes();
Route::get('/admin/user/roles',['middleware'=>['role', 'auth', 'web'], function(){
    return "Middleware roles";
}]);
Route::get('/dashboard', 'DashboardController@index')->name('dashboardHome');
Route::get('/dashboard/projectReview/{id}', 'DashboardController@showprojectReview')->name('projectReview');
Route::post('/dashboard', 'DashboardController@addStatus')->name('addStatus');
Route::get('/dashboard/project-list', 'DashboardController@projectList');
//ove dve rute izbrisi kao i sve funkcije vezane za to
Route::get('/dashboard/project-activation', 'DashboardController@projectActivation')->name('projectActivation');
Route::post('/dashboard/enter-coordinates', 'DashboardController@enterCoordinates')->name('enterCoordinates');
//convert to pdf
Route::get('dashboard/pdf', 'DashboardController@pdf');
// language
Route::get('/set-language/{lang}', 'PagesController@set')->name('set.language');
});