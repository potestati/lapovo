@extends('layouts.dashboard')

@section('content')
@include('inc.sidebar')
<pre>
{{var_dump($projects)}}
<pre>
    <div class="col-sm-9">
        <div class="main">
         <form method="POST" action="#" novalidate>
                @csrf
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Ime projekta</th>
                        <th scope="col">status</th>
                        <th scope="col">Vrednost projekta</th>
                        <th scope="col">Kategorija projekta</th>
                        <th scope="col">Oblast poslovanja projekta</th>
                        <th scope="col">Uneti tacan Geo URI</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($projects as $index=>$project)
                        <tr>
                            <th scope="row">{{++$index}}</th>
                            <td>{{$project->projectName}}</td>
                            <td>{{$project->status}}</td>
                            <td>{{$project->projectValue}}</td>
                            <td>{{$project->categoryName}}</td>
                            <td>{{$project->areaName}}</td>
                        </tr>  
                    @endforeach
                </tbody>
            </table>
        </form>
        </div>
    </div>
    <!-- Start Script-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
       
    <!-- Popper JS -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    {{--  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>  --}}
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Main Script -->
    <script src="js/main.js" type="text/javascript"></script>
        
@endsection
