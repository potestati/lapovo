@extends('layouts.app')

@section('content')
<style>
.row{
    float: left;
    width: 100%;
}
</style>

<div class="container-fluid show-upit">
    <div class="container">
        <h1 style="text-align: center;">{{ __('messages.Poziv') }}</h1>
        <p>{{ __('messages.postovani') }}</p>
        <p>{{ __('messages.Cilj') }}</p>
        <p>{{ __('messages.c') }} </p>
        <p>{{ __('messages.Struktura') }}</p>
        <ul>
            <li>{{ __('messages.map1') }}</li>
            <li>{{ __('messages.map2') }}</li>
            <li>{{ __('messages.map3') }}</li>
        </ul>
        <p>{{ __('messages.Klikom') }}</p>
        <ul>
            <li>{{ __('messages.jedna') }}</li>
            <li>{{ __('messages.do') }}</li>
        </ul>
        <p style="text-align: center;">{{ __('messages.sve') }}</p>
        <ul>
            <li style="color: red; list-style: none;">{{ __('messages.pređite') }}</li>
        </ul>
        <div class="formBox">
            <form  method="POST" action="{{ route('addUpit') }}" novalidate>
                @csrf
                <div class="row">
                    <br><br>
                    <div class="col-sm-12">
                        <h2 style="text-align: center;">{{ __('messages.služi') }}</h2>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('messages.Naziv') }}</div>
                            <br><br>
                            <textarea id="projectName" type="text" class="form-control{{ $errors->has('projectName') ? ' is-invalid' : '' }}" name="projectName" value="{{ old('projectName') }}" required autofocus></textarea>

                            @if ($errors->has('projectName'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('projectName') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('messages.Vrednost') }}</div>
                            <br><br>
                            <textarea id="projectValue" type="text" class="form-control{{ $errors->has('projectValue') ? ' is-invalid' : '' }}" name="projectValue" value="{{ old('projectValue') }}" required autofocus></textarea>

                            @if ($errors->has('projectValue'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('projectValue') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('messages.Opis') }}</div>
                            <br><br>
                            <textarea id="projectDescription" type="text" class="form-control{{ $errors->has('projectDescription') ? ' is-invalid' : '' }}" name="projectDescription" value="{{ old('projectDescription') }}" required autofocus></textarea>

                            @if ($errors->has('projectDescription'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('projectDescription') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('messages.datum') }}</div>
                            <br><br>
                            <br><br>
                            <textarea id="companyDescr" type="text" class="form-control{{ $errors->has('companyDescr') ? ' is-invalid' : '' }}" name="companyDescr" value="{{ old('companyDescr') }}" required autofocus></textarea>

                            @if ($errors->has('companyDescr'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('companyDescr') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('messages.regiona') }}</div>
                            <br><br>
                            <textarea id="regionDescr" type="text" class="form-control{{ $errors->has('regionDescr') ? ' is-invalid' : '' }}" name="regionDescr" value="{{ old('regionDescr') }}" required autofocus></textarea>

                            @if ($errors->has('regionDescr'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('regionDescr') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('messages.projekta') }}</div>
                                <br><br>
                                <textarea id="projectManager" type="text" class="form-control{{ $errors->has('projectManager') ? ' is-invalid' : '' }}" name="projectManager" value="{{ old('projectManager') }}" required autofocus></textarea>

                                @if ($errors->has('projectManager'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('projectManager') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <div class="inputText">{{ __('messages.regulativa') }}</div>
                                <br><br>
                                <textarea id="legalRegulations" type="text" class="form-control{{ $errors->has('legalRegulations') ? ' is-invalid' : '' }}" name="legalRegulations" value="{{ old('legalRegulations') }}" required autofocus></textarea>

                                @if ($errors->has('legalRegulations'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('legalRegulations') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <div class="inputText">{{ __('messages.Potencijal') }}</div>
                                <br><br>
                                <textarea id="projectPotencial" type="text" class="form-control{{ $errors->has('projectPotencial') ? ' is-invalid' : '' }}" name="projectPotencial" value="{{ old('projectPotencial') }}" required autofocus></textarea>

                                @if ($errors->has('projectPotencial'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('projectPotencial') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <div class="inputText">{{ __('messages.informacije') }}</div>
                                <br><br>
                                <textarea id="contactData" type="text" class="form-control{{ $errors->has('contactData') ? ' is-invalid' : '' }}" name="contactData" value="{{ old('contactData') }}" required autofocus></textarea>

                                @if ($errors->has('contactData'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('contactData') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <div class="form-group">
                                    <label for="#">{{ __('messages.OBLAST') }}</label>
                                    <select name="businessArea" multiple class="form-control" id="exampleFormControlSelect2">
                                    @foreach($oblasts as $oblast)
                                    @if(App::isLocale('en'))
                                        <option value="{{$oblast->poslovnaOblast}}">{{$oblast->areaName}}</option>
                                    @else
                                        <option value="{{$oblast->poslovnaOblast}}">{{strtoupper($oblast->poslovnaOblast)}}</option>
                                    @endif
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <div class="form-group">
                                    <label for="#">{{ __('messages.SKALA') }}</label>
                                    <select name="scaleValue" class="form-control" id="exampleFormControlSelect1">
                                    @foreach($values as $value)
                                        <option value="Od {{$value->minValue}} do {{$value->maxValue}}">Od {{$value->minValue}} do {{$value->maxValue}} &#8364;</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div>
                                </div>
                            </div>
                            <div class="row">
                                <div  style="margin-bottom: 40px;" class="col-sm-12">
                                    <input type="submit" name="#" class="button" value="{{ __('Send Message / Posalji podatke)') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                <input type="hidden" value="{{$projectId}}" name="projectId">
                </form>
            </div>
        </div>
    </div>
    <!-- Start Script-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Main Script -->
    {{--  <script src="js/main.js" type="text/javascript"></script>  --}}
    <script type="text/javascript">
        $(".input").focus(function () {
            $(this).parent().addClass("focus");
        })
    </script>
    <!-- / Script-->
    @endsection
