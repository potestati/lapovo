<!-- ovo je content za single upit-->
@extends('layouts.app')
@section('content')
<a class="btn btn-info" href="/upiti">Go back</a>
<br><br>
<h1>{{$upit->projectName}}</h1>
<hr>
<small>Project Name : {{$upit->projectName}}</small>
<small>Project Value : {{$upit->projectValue}}</small>
<small>Project Description : {{$upit->projectDescription}}</small>
<small>Company Description : {{$upit->companyDescr}}</small>
<small>Region Description : {{$upit->regionDescr}}</small>
<small>Project Manager : {{$upit->projectManager}}</small>
<small>Legal Regulations : {{$upit->legalRegulations}}</small>
<small>Project Potencial : {{$upit->projectPotencial}}</small>
<small>Contact Data : {{$upit->contactData}}</small>
<small>Business Area : {{$upit->businessArea}}</small>
<small>Scale Value : {{$upit->scaleValue}}</small>
<small>Category : {{$upit->category}}</small>

{{-- 
<div>
    <!-- ako hocemo da ckeditor parsira html onda moramo staviti jednu viticastu i uzvicnike -->
    {!!$upit->body!!}
</div>
<small>Written on {{$upit->created_at}}</small>
--}}
<hr>
<a href="/upiti/{{$upit->id}}/edit" class="btn btn-info">Edit</a>

{!!Form::open(['action' => ['UpitiController@destroy' , $upit->id], 'method' => 'POST', 'class' => 'float-right'])!!}
    {{Form::hidden('_method', 'DELETE')}}
    {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
{!! Form::close() !!}
@endsection