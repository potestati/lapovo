@extends('layouts.modal')
@section('content')
<div class="single">
<div style="float:left;" class="col-md-4">
<p>{{ __('messages.Selektovana') }}</p>
<h2>{{$selectedArea->poslovnaOblast}}</h2>
<br>
<h2>{{$selectedArea->areaName}}</h2>
<img src="/images/{{$selectedProject->path}}" alt="">
</div>
<div style="float:left;" class="col-md-4">
<p>{{ __('messages.projekat') }}</p>
<h2>{{$selectedProject->projectName}}</h2>
<ul>
    <h3>{{ __('messages.Podaci') }}</h3>
    <li>{{ __('messages.Oblast') }}<span>{{$selectedProject->projectSector}}</span></li>
    <li>{{ __('messages.Investicija') }}<span>{{$selectedProject->selectedMap}}</span></li>
    <li>{{ __('messages.Menadžer') }}<span>{{$selectedProject->projectManager}}</span></li>
    <li>{{ __('messages.Adresa') }} <span>{{$selectedProject->address}}</span></li>
    <li>{{ __('messages.Description') }}<span>{{$selectedProject->projectDescription}}</span></li>
    <li>{{ __('messages.Specifične') }}<span>{{$selectedProject->projectCharacter}}</span></li>
    <li>{{ __('messages.Karakteristike') }}<span>{{$selectedProject->regionCharacter}}</span></li>
    <li>{{ __('messages.Saradnja') }}<span>{{$selectedProject->offeredCooperation}}</span></li>
    <li>{{ __('messages.Sertifikati') }}<span>{{$selectedProject->certificates}}</span></li>
    <li class="vrednost-projekta">
    {{ __('messages.izrazeno') }}
    <span>{{$selectedProject->exact_value}},00 eur</span>
    </li>
    <li class="contact-data osoba">{{ __('messages.osoba') }}<span>{{$selectedProject->contactPerson}}</span></li>
    <li class="contact-data">{{ __('messages.Tel') }}<span>{{$selectedProject->contactData}}</span></li>
    <li class="contact-data">Website : <span>{{$selectedProject->website}}</span></li>
    <li class="contact-data">Email : <span>{{$selectedProject->email}}</span></li>
</ul>
<ul class="personal-data">
    <h3>{{ __('messages.licu') }}</h3>
    <h4>{{ __('messages.Vlasništvo') }}</h4>
    <li>{{ __('messages.Ime') }}<span>{{$userData->fullName}}</span></li>
    <li>{{ __('messages.kompanij') }}<span>{{$userData->companyName}}</span></li>
    <li>{{ __('messages.MB') }}<span>{{$userData->mb}}</span></li>
    <li>{{ __('messages.PIB') }}<span>{{$userData->pib}}</li>
    <li>{{ __('messages.kontaktiranje') }}<span> {{$userData->contactPerson}}</span></li>
    <li>Website : <span>{{$userData->website}}</span></li>
    <li>{{ __('messages.Adresa') }} : <span>{{$userData->address}}</span></li>
    <li>Email : <span>{{$userData->email}}</span></li>
</ul>
</div>
<div style="float:left;" class="col-md-4">
<p>{{ __('messages.Odabrana') }}</p>
<h2>{{$selectedCategory->imeKategorije}}</h2>
<br>
<h2>{{$selectedCategory->categoryName}}</h2>
<br>
<div class="proba-mape-3 PROBA">
    <script>
        window.znamo_lokaciju = true; 
        window.novcic_latitude = 45.3704; // na primer 45.3704
        window.novcic_longitude = 20.3958; // na primer 20.3958
        // ovo je ya vise novica i ovo koristimo sada
        window.vise_novcica = [
            @foreach($tackeNaMapi as $item)
                {znamo_lokaciju: true,
                novcic_latitude: {{$item['geoUri'][0]}},
                novcic_longitude: {{$item['geoUri'][1]}},
                novcic_click_otvori_url: "{{$item['popupOtvoriUrl']}}",
                novcic_popup_slika: "{{$item['popupSlikaUrl']}}",
                novcic_popup_naslov: "{{$item['popupNaslov']}}"
            },
                @endforeach
            ];
        </script>
       <div class="mapa-zrenjanin-projects" id="mapid" style="width: 400px; height: 600px;"></div>
    </div>
</div>
</div>
<hr style="float:left; width:100%;">
<div class="col-md-12 single-project">
</div>
</div>
@endsection