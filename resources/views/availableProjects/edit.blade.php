@extends('layouts.app')
@section('content')
<h1>Edit Available Projects</h1>
{!! Form::open(['action' => ['AvailableProjectsController@update', $availableProject->id], 'method' => 'POST']) !!}
<div class="form-group">
    {{Form::label('projectName', 'Project Name')}}
    {{Form::text('projectName', $availableProject->title, ['class' => 'form-control', 'placeholder' => 'Project Name'])}}
</div>
<div class="form-group">
    {{Form::label('status', 'Status of the project')}}
    {{Form::text('status', $availableProject->title, ['class' => 'form-control', 'placeholder' => 'Status of the project'])}}
</div>
<div class="form-group">
    {{Form::label('category', 'Category of the project')}}
    {{Form::text('category', $availableProject->title, ['class' => 'form-control', 'placeholder' => 'Category of the project'])}}
</div>
<div class="form-group">
    {{Form::label('amount', 'Value of the project')}}
    {{Form::text('amount', $availableProject->title, ['class' => 'form-control', 'placeholder' => 'Value of the project'])}}
</div>
{{--<div class="form-group">
    {{Form::label('body', 'Body')}}
    {{Form::textarea('body', $availableProject->body, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body'])}}
</div>--}}
    {{-- kada ovo radimo treba nam route , artisan php route:list
    pa onda u tom pregledu lista vidimo 
AvailableProjectsController@show     | web          |
|        | PUT|PATCH | posts/{post} 
zbog toga nam treba metod put , ne mozemo menjati onaj gore metod post
ali mozemo staviti skriveno polje sa PUT metodom za update--}}
{{Form::hidden('_method', 'PUT')}}
{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
<!-- nakon post request vodi nas na funkciju edit u AvailableProjectsController-->
{!! Form::close() !!}

@endsection