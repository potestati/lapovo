@extends('layouts.app')
@section('content')
<h1>Create Available Projects</h1>
{!! Form::open(['action' => 'AvailableProjectsController@store', 'method' => 'POST']) !!}
<div class="form-group">
    {{Form::label('projectName', 'Project Name')}}
    {{Form::text('projectName', '', ['class' => 'form-control', 'placeholder' => 'Project Name'])}}
</div>
<div class="form-group">
    {{Form::label('status', 'Status of the project')}}
    {{Form::text('status', '', ['class' => 'form-control', 'placeholder' => 'Status of the project'])}}
</div>
<div class="form-group">
    {{Form::label('category', 'Category of the project')}}
    {{Form::text('category', '', ['class' => 'form-control', 'placeholder' => 'Category of the project'])}}
</div>
<div class="form-group">
    {{Form::label('amount', 'Value of the project')}}
    {{Form::text('amount', '', ['class' => 'form-control', 'placeholder' => 'Value of the project'])}}
</div>
    {{--<div class="form-group">
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body'])}}
        </div>--}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
        <!-- nakon AvailableProjects request vodi nas na funkciju store u AvailableProjectsController-->
        {!! Form::close() !!}

        @endsection