@extends('layouts.modal')

@section('content')

<div class="available-projects">
<h1>GIM - Geografska informativna mapa Zrenjanin</h1>

@if(count($projects) > 0)
<div class="col-md-4">
<p class="cat-name">
Odabrana kategorija <br>
{{$projectCategory->imeKategorije}}
</p>
<ul>
@foreach($projects as $project)
    @if($project->status == 1)
    <li>
        <a href="/category/{{$projectCategory->slug}}/area/{{$projectOblast->slug}}/project/{{$project->slug}}" style="{{$project->status == 1 ? 'color: red' : 'color: gray'}}">
            {{$project->projectName}}
        </a>
    </li>
    @else
    <li style="color: gray; text-transform: capitalize;">
            {{$project->projectName}}
    </li>
    @endif
@endforeach
</ul>
</div>
@endif
</div>
<div class="col-md-4 projects-map">
<div class="proba-mape-3 PROBA">
    <script>
        // podaci za koordinate novcica. 
        // php mora da ih ovde upise
        window.znamo_lokaciju = true; 
        window.novcic_latitude = 45.3704; // na primer 45.3704
        window.novcic_longitude = 20.3958; // na primer 20.3958
        // ovo je ya vise novica i ovo koristimo sada
        window.vise_novcica = [
            @foreach($geoUri as $item)
                {znamo_lokaciju: true,
                novcic_latitude: {{$item[0]}},
                novcic_longitude: {{$item[1]}}},
                @endforeach
            ];
        </script>
       <div class="mapa-zrenjanin-projects" id="mapid" style="width: 400px; height: 600px;"></div>
    </div>
</div>
<div class="col-md-4 skale-vrednosti">
<p class="cat-name">
Selektovana oblast <br>
{{$projectOblast->poslovnaOblast}}
</p>
@if(count($scaleOfValues)>0)
<h2>Pronadjite projekat po skali vrednosti</h2>
<ul style="list-style: none;">
@foreach($scaleOfValues as $value)
@if($collection->contains($value->id))
<li class="">
<a style="color:red;" href="{{'/area/' . $projectOblast->slug . '/value/' . $value->slug}}">
OD {{strtoupper($value->minValue)}} DO {{strtoupper($value->maxValue)}} EUR
<br><br>
</a>
</li>
@else
<li  style="color:grey;" class="">
OD {{strtoupper($value->minValue)}} DO {{strtoupper($value->maxValue)}} EUR
<br><br>
</li>
@endif
@endforeach
</ul>
@endif
</div>
<div class="scale-values-cat">
<ul class="other-categories">
<p>Ovde izaberite drugu kategoriju projekata &#8250;&#8250;&#8250;</p>
    @if(count($categories) > 0)
        @foreach($categories as $oneCategory) 
            @if($oneCategory->id !== $projectCategory->id)
                <li>
                <h4>{{$oneCategory->imeKategorije}}</h4>
                    <a href="/category/{{$oneCategory->slug}}">
                        <img src="{{asset('img/'.$oneCategory->categoryImage)}}" alt="">
                    </a>
                </li>
            @endif
        @endforeach
    @endif
</ul>
</div>
@endsection
