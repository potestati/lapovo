@extends('layouts.dashboard')

@section('content')

<div class="container-fluid">
<div class="container approve-project">
    <h2>Ulogovani ste kao Administrator</h2>
    <p>
        Kako bi se prikazao novčić na mapi koji označava raspoložive projekte,
        potrebno je da administrator unese tačne koordinate lokacije na kojoj se projekat nalazi.
    </p>
    <p>
        Na url adresi
    </p>
    <a href="https://www.openstreetmap.org/#map=12/44.1855/21.0447" target="_blank">
    https://www.openstreetmap.org/#map=12/44.1855/21.0447 
    </a>
    <p>možete pronaći Open Site Map aplikaciju.</p>
    <p>Nakon unosa naziva ulice kliknuti na dugme GO ili IDI kako bi se pronasla lokacija na mapi date ulice.</p>
    <img class="geouri-share" src="{{asset('img/geouri-app.jpg')}}" alt="">
    <p>Nakon ovoga potrebno je, sa desne strane, naći Share dugme</p>
    <img class="" src="{{asset('img/share-button.jpg')}}" alt=""> 
    <p>kako bi se otvorio deo za prikaz koordinata koje je potrebno kopirati bez razmaka sa strane.</p>
    <img class="geouri-share" src="{{asset('img/geouri.jpg')}}" alt="">
    <p>Kada program nadje adresu sa leve strane aplikacije je potrebno otvoriti podatke o lokaciji na Share dugme.</p>
    <p>Adresa se mora kopirati tačno i bez izmene karaktera kao i bez dodavanja razmaka sa strane.</p>
    <p>Kopira se deo oznake koordinata koji se nalazi ispod naslova Geo Uri.</p>
    <img class="geouri-share" src="{{asset('img/site-map.jpg')}}" alt="">
</div>
    <br>
    <div class="container">
        <div class="formBox">
            <form method="POST" action="{{ route('createAvailableProjects') }}" novalidate>
                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <h1>Aktiviranje projekta za apliciranje</h1>
                        <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="form-group">
                                <label for="#">{{ __('Lista odobrenih projekata') }}</label>
                                <select name="project" multiple class="form-control" id="exampleFormControlSelect2">
                                    @foreach ($approvedProjects as $approvedProject)
                                    <option value="{{$approvedProject->id}}">{{$approvedProject->projectName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('GEO URI / GEO URI') }}</div>
                            <input id="geo_uri" type="text" class="input{{ $errors->has('geo_uri') ? ' is-invalid' : '' }}" name="geo_uri" value="" required autofocus>
                            @if ($errors->has('geo_uri'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('geo_uri') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <input type="submit" name="#" class="button" value="{{ __('messages.posalji') }}">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Start Script-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Main Script -->
{{--  <script src="js/main.js" type="text/javascript"></script>  --}}

<!-- / Script-->

@endsection
