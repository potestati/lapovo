@extends('layouts.app')

@section('content')
<div class="jumbotron text-center">
<p style="text-align : center;">
{{ __('messages.dobrodosli') }} 
<a href="#" style="color: red; font-weight: 500;">{{ __('messages.invest') }} </a>
{{ __('messages.koja') }} 
</p>
<p>
{{ __('messages.uputstvo') }} 
</p>
<p>
{{ __('messages.pretraga') }} 
</p>
<p>
{{ __('messages.kliknite') }} <a style="color: red; font-weight: 500;" href="/project-categories">{{ __('messages.ovde') }}</a>
</p>
<p>
{{ __('messages.ukoliko') }}
<a style="color: red; font-weight: 500;" href="/register">{{ __('messages.ovde') }}</a>
</p>
<p></p>
<p>
    <a class="btn btn-primary btn-success" href="/register">{{ __('messages.register') }}</a>
    <a class="btn btn-primary btn-primary" href="/login">{{ __('messages.login') }}</a> 
</p>
</div>
        <!-- Start Script-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
       
        <!-- Popper JS -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Main Script -->
        <script src="js/main.js" type="text/javascript"></script>
@endsection

