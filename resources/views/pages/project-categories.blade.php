@extends('layouts.app')

@section('content')

<br><br>
@if(count($categories) > 0)
<h2 class="cat-heading">{{ __('messages.odaberite') }}</h2>
<hr>
<ul style="list-style: none;" class="categories-list">
    @foreach($categories as $category)
        <li class="maps-zrenjanin" style="display: inline-block; float: left; width: 30%;" >
            <h4>{{ $category->imeKategorije }}</h4>
            <hr>
            <a href="/category/{{$category->slug}}">
                <img src="{{asset('img/'.$category->categoryImage)}}" alt="">
            </a>
            <hr>
            <h4>{{ $category->categoryName }}</h4>
            <hr>
        </li>
    @endforeach
</ul>
@endif


<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

<!-- Main Script -->
<script src="js/main.js" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script type="text/javascript">
    $(".input").focus(function () {
        $(this).parent().addClass("focus");
    })
</script>
    <!-- / Script-->
@endsection

