@extends('layouts.oblasti')

@section('content')

<h2 class="invest-heading">
{{ __('messages.GIM') }} 
</h2>

@if(count($oblasts) > 0)
<div class="oblasti-strana">
<p class="choosen-cat">{{$category->imeKategorije}} / {{$category->categoryName}}</p>
<div class="col-md-4">
<h3>{{ __('messages.INVESTICIONIH') }}</h3>
    <ul class="oblasti" style="list-style: none;">
        @foreach($oblasts as $oneOblast)
        @if($collection->contains($oneOblast->id))
            <li class="one-oblast">
                <a style="color: red;" href="{{'/category/' . $category->slug . '/business-area/' . $oneOblast->slug}}">
                    {{strtoupper($oneOblast->areaName)}} <br>
                    {{strtoupper($oneOblast->poslovnaOblast)}}
                </a>
            </li>
        @else
            <li class="one-oblast" style="color: grey;">
                {{strtoupper($oneOblast->areaName)}} <br>
                {{strtoupper($oneOblast->poslovnaOblast)}}
            </li>
        @endif 
        @endforeach
    </ul>
    @endif
</div>
<div class="col-md-4">
    <div class="proba-mape-1 PROBA">
    <script>

        // podaci za koordinate novcica. 
        // php mora da ih ovde upise
        window.znamo_lokaciju = true; // ukoliko je chekirano checkbox da je GEOURI ISPRAVAN onda treba da bude true a u svakom drugom slucaju false.
        window.novcic_latitude = 45.3704; // na primer 45.3704
        window.novcic_longitude = 20.3958; // na primer 20.3958
        // ovo je ya vise novica i ovo koristimo sada
                //kljuc ispod ide pod navodnike jer je vrednost u bazi string
                //kljuc ovde ne ide pod navodnike jer je int u bazi
        window.vise_novcica = [
            @foreach($tackeNaMapi as $item)

                {znamo_lokaciju: true,
                novcic_latitude: {{$item['geoUri'][0]}},
                novcic_longitude: {{$item['geoUri'][1]}},
                novcic_click_otvori_url: "{{$item['popupOtvoriUrl']}}",
                novcic_popup_slika: "{{$item['popupSlikaUrl']}}",
                novcic_popup_naslov: "{{$item['popupNaslov']}}"
            },


                @endforeach
            ];
            console.log("test blade ");
            console.log(window.vise_novcica);
        </script>
       <div class="mapa-zrenjanin" id="mapid" style="width: 400px; height: 800px;"></div>
    </div>
</div>
<div class="col-md-4">
<h3>{{ __('messages.VREDNOSTI') }}</h3>
@if(count($scaleValues) > 0)
    <ul class="investicioni-projekti" style="list-style: none;">
    @foreach($scaleValues as $scaleValue)
        @if($collectionOfValues->contains($scaleValue->id))
            <li class="investicioni">
                <a style="color: red;" href="{{'/category/' . $category->slug . '/scale-of-values/' . $scaleValue->slug}}">
                OD {{strtoupper($scaleValue->minValue)}} DO {{strtoupper($scaleValue->maxValue)}} EUR
                <br>
                FROM {{strtoupper($scaleValue->minValue)}} TO {{strtoupper($scaleValue->maxValue)}} EUR
                </a>
            </li>
        @else
            <li class="investicioni" style="color: grey;">
                OD {{strtoupper($scaleValue->minValue)}} DO {{strtoupper($scaleValue->maxValue)}} EUR
                <br>
                FROM {{strtoupper($scaleValue->minValue)}} TO {{strtoupper($scaleValue->maxValue)}} EUR
            </li>
        @endif 
        @endforeach
    </ul>
@endif
</div>
</div>
<div class="back-to-home">
<a class="" href="/">HOME</a>
</div>
<ul class="other-categories business-area-page">
<p>{{ __('messages.izaberite') }} &#8250;&#8250;&#8250;</p>
    @if(count($categories) > 0)
        @foreach($categories as $oneCategory) 
            @if($oneCategory->id !== $category->id)
                <li>
                <h4>{{$oneCategory->imeKategorije}}</h4>
                    <a href="/category/{{$oneCategory->slug}}">
                        <img src="{{asset('img/'.$oneCategory->categoryImage)}}" alt="">
                    </a>
                    <h4>{{$oneCategory->categoryName}}</h4>
                </li>
            @endif
        @endforeach
    @endif
</ul>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<!-- Main Script -->
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script type="text/javascript">
    $(".input").focus(function () {
        $(this).parent().addClass("focus");
    })
</script>
@endsection
