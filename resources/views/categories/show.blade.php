<!-- ovo je content za single category-->
@extends('layouts.app')
@section('content')
<a class="btn btn-info" href="/categories">Go back</a>
<br><br>
<h1>{{$category->categoryName}}</h1>
<hr>
<small>Category name {{$category->categoryName}}</small>
<small>Category value {{$category->categoryValue}}</small>
<small>Category description {{$category->categoryDescription}}</small>
<small>Category manager {{$category->categoryManager}}</small>
<small>Category regulations {{$category->legalRegulations}}</small>
<small>Category potencial {{$category->categoryPotencial}}</small>
<small>Category data {{$category->contactData}}</small>
<small>Category area {{$category->businessArea}}</small>
<small>Category value {{$category->scaleValue}}</small>
{{--<div>
    <!-- ako hocemo da ckeditor parsira html onda moramo staviti jednu viticastu i uzvicnike -->
    {!!$category->body!!}
</div>
--}}
<small>Written on {{$category->created_at}}</small>
<hr>
<a href="/categories/{{$category->id}}/edit" class="btn btn-info">Edit</a>

{!!Form::open(['action' => ['CategoriesController@destroy' , $category->id], 'method' => 'POST', 'class' => 'float-right'])!!}
    {{Form::hidden('_method', 'DELETE')}}
    {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
{!! Form::close() !!}
@endsection