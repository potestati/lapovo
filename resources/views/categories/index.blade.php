@extends('layouts.app')
@section('content')
<h1>Categories</h1>
@if(count($categories) > 0)
@foreach($categories as $category)
<div class="card card-body bg-light">
<h3><a href="/categories/{{$category->id}}">{{$category->categoryName}}</a></h3>
<small>Category name {{$category->categoryName}}</small>
<small>Category value {{$category->categoryValue}}</small>
<small>Category description {{$category->categoryDescription}}</small>
<small>Category manager {{$category->categoryManager}}</small>
<small>Category regulations {{$category->legalRegulations}}</small>
<small>Category potencial {{$category->categoryPotencial}}</small>
<small>Category data {{$category->contactData}}</small>
<small>Category area {{$category->businessArea}}</small>
<small>Category value {{$category->scaleValue}}</small>
</div>
@endforeach
<!-- ovo sluzi za paginaciju -->
{{$categories->links()}}
@else
<p>There is no categories</p>
@endif
@endsection