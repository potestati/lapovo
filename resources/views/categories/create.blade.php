@extends('layouts.app')
@section('content')
<h1>Create Category</h1>
{!! Form::open(['action' => 'CategoriesController@store', 'method' => 'POST']) !!}
<div class="form-group">

            {{Form::label('categoryName', 'Category Name')}}
            {{Form::text('categoryName', '', ['class' => 'form-control', 'placeholder' => 'Category Name'])}}
        </div>
        <div class="form-group">
        	{{Form::label('categoryValue', 'Category Value')}}
        	{{Form::text('categoryValue', '', ['class' => 'form-control', 'placeholder' => 'Category Value'])}}
        </div>
        <div class="form-group">
        	{{Form::label('categoryDescription', 'Category Description')}}
        	{{Form::text('categoryDescription', '', ['class' => 'form-control', 'placeholder' => 'Category Description'])}}
        </div>
        <div class="form-group">
        	{{Form::label('categoryManager', 'Category Manager')}}
        	{{Form::text('categoryManager', '', ['class' => 'form-control', 'placeholder' => 'Category Manager'])}}
        </div>
        <div class="form-group">
        	{{Form::label('legalRegulations', 'Legal Regulations')}}
        	{{Form::text('legalRegulations', '', ['class' => 'form-control', 'placeholder' => 'Legal Regulations'])}}
        </div>
        <div class="form-group">
        	{{Form::label('categoryPotencial', 'Category Potencial')}}
        	{{Form::text('categoryPotencial', '', ['class' => 'form-control', 'placeholder' => 'Category Potencial'])}}
        </div>
        <div class="form-group">
        	{{Form::label('contactData', 'Contact Data')}}
        	{{Form::text('contactData', '', ['class' => 'form-control', 'placeholder' => 'Contact Data'])}}
        </div>
        <div class="form-group">
        	{{Form::label('businessArea', 'Business Area')}}
        	{{Form::text('businessArea', '', ['class' => 'form-control', 'placeholder' => 'Business Area'])}}
        </div>
        <div class="form-group">
        	{{Form::label('scaleValue', 'Scale Value')}}
        	{{Form::text('scaleValue', '', ['class' => 'form-control', 'placeholder' => 'Scale Value'])}}
        </div>
        {{--<div class="form-group">
        	{{Form::label('body', 'Body')}}
        	{{Form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body'])}}
        </div>--}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}

        {!! Form::close() !!}

        @endsection