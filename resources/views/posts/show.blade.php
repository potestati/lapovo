<!-- ovo je content za single post-->
@extends('layouts.app')
@section('content')
<a class="btn btn-info" href="/posts">Go back</a>
<br><br>
<h1>{{$post->title}}</h1>
<hr>
<small>Written on {{$post->created_at}}</small>
<div>
    <!-- ako hocemo da ckeditor parsira html onda moramo staviti jednu viticastu i uzvicnike -->
    {!!$post->body!!}
</div>
<small>Written on {{$post->created_at}}</small>
<hr>
<a href="/posts/{{$post->id}}/edit" class="btn btn-info">Edit</a>
<!-- nakon submitovanja izmena akcija je na update funkciju u PostControlleru-->
{!!Form::open(['action' => ['PostsController@destroy' , $post->id], 'method' => 'POST', 'class' => 'float-right'])!!}
    {{Form::hidden('_method', 'DELETE')}}
    {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
{!! Form::close() !!}

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endsection