@extends('layouts.app')
@section('content')
<h1>Posts</h1>
@if(count($posts) > 0)
@foreach($posts as $post)
<div class="card card-body bg-light">
	<h3><a href="/posts/{{$post->id}}">{{$post->title}}</a></h3>
	<small>Written on {{$post->created_at}}</small>
</div>
@endforeach
<!-- ovo sluzi za paginaciju -->

{{$posts->links()}}
@else
<p>There is no posts</p>
@endif

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endsection