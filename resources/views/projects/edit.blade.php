@extends('layouts.app')
@section('content')
<h1>Edit Project</h1>
{!! Form::open(['action' => ['ProjectsController@update', $project->id], 'method' => 'POST']) !!}
<div class="form-group">
    {{Form::label('projectName', 'Project Name')}}
    {{Form::text('projectName', $project->projectName, ['class' => 'form-control', 'placeholder' => 'Project Name'])}}
</div>
<div class="form-group">
    {{Form::label('projectSector', 'Project Sector')}}
    {{Form::text('projectSector', $project->projectSector, ['class' => 'form-control', 'placeholder' => 'Project Sector'])}}
</div>
<div class="form-group">
    {{Form::label('projectValue', 'Project Value')}}
    {{Form::text('projectValue', $project->projectValue, ['class' => 'form-control', 'placeholder' => 'Project Value'])}}
</div>
<div class="form-group">
    {{Form::label('selectedMap', 'Selected Map')}}
    {{Form::text('selectedMap', $project->selectedMap, ['class' => 'form-control', 'placeholder' => 'Selected Map'])}}
</div>
<div class="form-group">
    {{Form::label('projectManager', 'Project Manager')}}
    {{Form::text('projectManager', $project->projectManager, ['class' => 'form-control', 'placeholder' => 'Project Manager'])}}
</div>
<div class="form-group">
    {{Form::label('contactData', 'Contact Data')}}
    {{Form::text('contactData', $project->contactData, ['class' => 'form-control', 'placeholder' => 'Contact Data'])}}
</div>
<div class="form-group">
    {{Form::label('website', 'Website')}}
    {{Form::text('website', $project->website, ['class' => 'form-control', 'placeholder' => 'Website'])}}
</div>
<div class="form-group">
    {{Form::label('email', 'Email')}}
    {{Form::text('email', $project->email, ['class' => 'form-control', 'placeholder' => 'Email'])}}
</div>
<div class="form-group">
    {{Form::label('address', 'Address')}}
    {{Form::text('address', $project->address, ['class' => 'form-control', 'placeholder' => 'Address'])}}
</div>
<div class="form-group">
    {{Form::label('projectCharacter', 'Project Character')}}
    {{Form::text('projectCharacter', $project->projectCharacter, ['class' => 'form-control', 'placeholder' => 'Project Character'])}}
</div>
<div class="form-group">
    {{Form::label('regionCharacter', 'Region Character')}}
    {{Form::text('regionCharacter', $project->regionCharacter, ['class' => 'form-control', 'placeholder' => 'Region Character'])}}
</div>
<div class="form-group">
    {{Form::label('offeredCooperation', 'Offered Cooperation')}}
    {{Form::text('offeredCooperation', $project->offeredCooperation, ['class' => 'form-control', 'placeholder' => 'Offered Cooperation'])}}
</div>
<div class="form-group">
    {{Form::label('certificates', 'Certificates')}}
    {{Form::text('certificates', $project->certificates, ['class' => 'form-control', 'placeholder' => 'Certificates'])}}
</div>
<div class="form-group">
    {{Form::label('contactPerson', 'Contact Person')}}
    {{Form::text('contactPerson', $project->contactPerson, ['class' => 'form-control', 'placeholder' => 'Contact Person'])}}
</div>
<div class="form-group">
    {{Form::label('category', 'Category')}}
    {{Form::text('category', $project->category, ['class' => 'form-control', 'placeholder' => 'Category'])}}
</div>
{{-- 
<div class="form-group">
    {{Form::label('body', 'Body')}}
    {{Form::textarea('body', $project->body, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body'])}}
</div>
--}}
    {{-- kada ovo radimo treba nam route , artisan php route:list
    pa onda u tom pregledu lista vidimo 
ProjectsController@show     | web          |
|        | PUT|PATCH | projects/{project} 
zbog toga nam treba metod put , ne mozemo menjati onaj gore metod project
ali mozemo staviti skriveno polje sa PUT metodom za update--}}
{{Form::hidden('_method', 'PUT')}}
{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
<!-- nakon project request vodi nas na funkciju edit u ProjectsController-->
{!! Form::close() !!}

@endsection