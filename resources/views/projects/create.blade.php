@extends('layouts.app')
@section('content')
<h1>Create Project</h1>
{!! Form::open(['action' => 'ProjectsController@store', 'method' => 'POST']) !!}
<div class="form-group">
    {{Form::label('projectName', 'Project Name')}}
    {{Form::text('projectName', '', ['class' => 'form-control', 'placeholder' => 'Project Name'])}}
</div>
<div class="form-group">
    {{Form::label('projectSector', 'Project Sector')}}
    {{Form::text('projectSector', '', ['class' => 'form-control', 'placeholder' => 'Project Secto'])}}
</div>
<div class="form-group">
    {{Form::label('projectValue', 'Project Value')}}
    {{Form::text('projectValue', '', ['class' => 'form-control', 'placeholder' => 'Project Value'])}}
</div>
<div class="form-group">
    {{Form::label('selectedMap', 'Selected Map')}}
    {{Form::text('selectedMap', '', ['class' => 'form-control', 'placeholder' => 'Selected Map'])}}
</div>
<div class="form-group">
    {{Form::label('projectManager', 'Project Manager')}}
    {{Form::text('projectManager', '', ['class' => 'form-control', 'placeholder' => 'Project Manager'])}}
</div>
<div class="form-group">
    {{Form::label('contactData', 'Contact Data')}}
    {{Form::text('contactData', '', ['class' => 'form-control', 'placeholder' => 'Contact Data'])}}
</div>
<div class="form-group">
    {{Form::label('website', 'Website')}}
    {{Form::text('website', '', ['class' => 'form-control', 'placeholder' => 'Website'])}}
</div>
<div class="form-group">
    {{Form::label('email', 'Email')}}
    {{Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'Email'])}}
</div>
<div class="form-group">
    {{Form::label('address', 'Address')}}
    {{Form::text('address', '', ['class' => 'form-control', 'placeholder' => 'Address'])}}
</div>
<div class="form-group">
    {{Form::label('title', 'Title')}}
    {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
</div>
<div class="form-group">
    {{Form::label('projectCharacter', 'Project Character')}}
    {{Form::text('projectCharacter', '', ['class' => 'form-control', 'placeholder' => 'Project Character'])}}
</div>
<div class="form-group">
    {{Form::label('regionCharacter', 'Region Character')}}
    {{Form::text('regionCharacter', '', ['class' => 'form-control', 'placeholder' => 'Region Character'])}}
</div>
<div class="form-group">
    {{Form::label('offeredCooperation', 'Offered Cooperation')}}
    {{Form::text('offeredCooperation', '', ['class' => 'form-control', 'placeholder' => 'Offered Cooperation'])}}
</div>
<div class="form-group">
    {{Form::label('certificates', 'Certificates')}}
    {{Form::text('certificates', '', ['class' => 'form-control', 'placeholder' => 'Certificates'])}}
</div>
<div class="form-group">
    {{Form::label('contactPerson', 'Contact Person')}}
    {{Form::text('contactPerson', '', ['class' => 'form-control', 'placeholder' => 'Contact Person'])}}
</div>
<div class="form-group">
    {{Form::label('category', 'Category')}}
    {{Form::text('category', '', ['class' => 'form-control', 'placeholder' => 'Category'])}}
</div>
{{-- 
<div class="form-group">
    {{Form::label('body', 'Body')}}
    {{Form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body'])}}
</div>
--}}
{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
<!-- nakon post request vodi nas na funkciju store u ProjectsController-->
{!! Form::close() !!}

@endsection