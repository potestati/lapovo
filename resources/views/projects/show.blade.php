<!-- ovo je content za single project-->
@extends('layouts.app')
@section('content')
<a class="btn btn-info" href="/projects">Go back</a>
<br><br>
<h1>{{$project->projectName}}</h1>
<hr>
{{--<small>Written on {{$project->created_at}}</small>--}}
<small>Project Name :  {{$project->projectName}}</small>
<small>Project Sector : {{$project->projectSector}}</small>
<small>Project Value : {{$project->projectValue}}</small>
<small>Selected Map : {{$project->selectedMap}}</small>
<small>Project Manager : {{$project->projectManager}}</small>
<small>Contact Data : {{$project->contactData}}</small>
<small>Website : {{$project->website}}</small>
<small>Email : {{$project->email}}</small>
<small>Address : {{$project->address}}</small>
<small>Project Character : {{$project->projectCharacter}}</small>
<small>Region Character : {{$project->regionCharacter}}</small>
<small>Offered Cooperation : {{$project->offeredCooperation}}</small>
<small>Certificates : {{$project->certificates}}</small>
<small>Contact Person : {{$project->contactPerson}}</small>
<small>Category : {{$project->category}}</small>
{{-- 
<div>
    {!!$project->body!!}
</div>
--}}
<small>Written on {{$project->created_at}}</small>
<hr>
<a href="/projects/{{$project->id}}/edit" class="btn btn-info">Edit</a>
<!-- nakon submitovanja izmena akcija je na update funkciju u ProjectsControlleru-->
{!!Form::open(['action' => ['ProjectsController@destroy' , $project->id], 'method' => 'POST', 'class' => 'float-right'])!!}
    {{Form::hidden('_method', 'DELETE')}}
    {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
{!! Form::close() !!}
@endsection