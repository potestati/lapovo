@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
    <div class="container">
        <div class="formBox izmena-projekta">
            <form class="editovanje" method="post" name="updateProject" action="{{ route('updateProject', $singleProject->id) }}" novalidate>
            <input name="category_id" id="prosledjeniCategoryID" type="hidden" value="{{ $singleProject->category_id }}">
                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <h1>Ovo je aplikacija korisnika {{$singleProject->user->fullName}}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox ">
                            <div class="inputText">{{ __('Project Name / Naziv projekta') }}</div>

                            <input id="projectName" type="text" class="input{{ $errors->has('projectName') ? ' is-invalid' : '' }}" 
                            name="projectName" value="{{ $singleProject->projectName }}" required autofocus>

                            @if ($errors->has('projectName'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('projectName') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <br>
                <h2>Map of Investments – Mapa Investicija</h2>
                <br>
                <br>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox ">
                            <div class="inputText">{{ __('Sector of Economy / Oblast') }}</div>
                            <input id="projectSector" type="text" class="input{{ $errors->has('projectSector') ? ' is-invalid' : '' }}" name="projectSector" value="{{ $singleProject->projectSector }}" required autofocus>
                            @if ($errors->has('projectSector'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('projectSector') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Project value (EUR) / Vrednost') }}</div>
                            <input id="exact_value" type="text" class="input{{ $errors->has('exact_value') ? ' is-invalid' : '' }}" name="exact_value" value="{{ $singleProject->exact_value}}" required autofocus>

                            @if ($errors->has('exact_value'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('exact_value') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('You apply with project as / Odabrana Mapa') }}</div>
                            <input id="selectedMap" type="text" class="input{{ $errors->has('selectedMap') ? ' is-invalid' : '' }}" name="selectedMap" value="{{ $singleProject->selectedMap }}" required autofocus>

                            @if ($errors->has('selectedMap'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('selectedMap') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Project Manager / Funkcija') }}</div>
                            <input id="projectManager" type="text" class="input{{ $errors->has('projectManager') ? ' is-invalid' : '' }}" name="projectManager" value="{{ $singleProject->projectManager}}" required autofocus>

                            @if ($errors->has('projectManager'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('projectManager') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Contact phone / Kontakt Tel: +381') }}</div>
                            <input id="contactData" type="text" class="input{{ $errors->has('contactData') ? ' is-invalid' : '' }}" name="contactData" value="{{ $singleProject->contactData }}" required autofocus>

                            @if ($errors->has('contactData'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('contactData') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Website') }}</div>
                            <input id="website" type="text" class="input{{ $errors->has('website') ? ' is-invalid' : '' }}" name="website" value="{{ $singleProject->website}}" required autofocus>

                            @if ($errors->has('website'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('website') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Email') }}</div>
                            <input id="email" type="email" class="input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $singleProject->email }}" required autofocus>

                            @if ($errors->has('email'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Address / Adresa') }}</div>
                            <input id="address" type="text" class="input{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ $singleProject->address}}" required autofocus>

                            @if ($errors->has('address'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">Description / Opis projekta</div>
                            <br><br>
                            <textarea id="projectDescription" type="text" class="input{{ $errors->has('projectDescription') ? ' is-invalid' : '' }}" name="projectDescription" value="" required autofocus>{{ $singleProject->projectDescription }}</textarea>
                            @if ($errors->has('projectDescription'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('projectDescription') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">Specify characteristics of the product / Specifične karakteristike proizvoda ili projekta ili patenta</div>
                            <br><br>
                            <textarea id="projectCharacter" type="text" class="input{{ $errors->has('projectCharacter') ? ' is-invalid' : '' }}" name="projectCharacter" value="" required autofocus>{{ $singleProject->projectCharacter }}</textarea>
                            @if ($errors->has('projectCharacter'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('projectCharacter') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">Specify characteristics of the region and the benefits that the region may have from a realization of the project /<br> Specifične karakteristike regiona i koristi koje bi Grad – Opština dobila realizacijom ovog projekta</div>
                            <br><br>
                            <textarea id="regionCharacter" type="text" class="input{{ $errors->has('regionCharacter') ? ' is-invalid' : '' }}" name="regionCharacter" value="" required autofocus>{{ $singleProject->regionCharacter}}</textarea>
                            @if ($errors->has('regionCharacter'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('regionCharacter') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">Cooperation offered to a foreign partner or investor /<br>Saradnja koju nudite stranom partner, odnosno investitoru</div>
                            <br><br>
                            <textarea id="offeredCooperation" type="text" class="input{{ $errors->has('offeredCooperation') ? ' is-invalid' : '' }}" name="offeredCooperation" value="" required autofocus>{{ $singleProject->offeredCooperation}}</textarea>
                            @if ($errors->has('offeredCooperation'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('offeredCooperation') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">Legal regulations (certificates, patent rights, land ownership, etc.)/<br>Pravni okvir i zakonodavna osnova predloga (sertifikati, patentna prava, vlasništvo nad zemljom, i drugo)</div>
                            <br><br>
                            <textarea id="certificates" type="text" class="input{{ $errors->has('certificates') ? ' is-invalid' : '' }}" name="certificates" value="" required autofocus>{{ $singleProject->certificates }}</textarea>
                            @if ($errors->has('certificates'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('certificates') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText"><i>Contact person/ Kontakt osoba</i></div>
                            <textarea id="contactPerson" type="text" class="input{{ $errors->has('contactPerson') ? ' is-invalid' : '' }}" name="contactPerson" value="" required autofocus>{{ $singleProject->contactPerson }}</textarea>
                            @if ($errors->has('contactPerson'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('contactPerson') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>

                <!-- <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('1.Naziv projekta (i svrstavanje u grupu – dato u prilogu)') }}</div>
                            <br><br>
                            <textarea id="projectName" type="text" class="form-control{{ $errors->has('projectName') ? ' is-invalid' : '' }}" name="projectName" value="" required autofocus>
                                {{ $singleProject->projectName}}
                            </textarea>

                            @if ($errors->has('projectName'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('projectName') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div> -->
                <!-- <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('2.Vrednost projekta (po skali -  dato na sledećoj strani)') }}</div>
                            <br><br>
                            <textarea id="projectValue" type="text" class="form-control{{ $errors->has('projectValue') ? ' is-invalid' : '' }}" name="projectValue" value="" required autofocus>
                                {{$singleProject->projectValue }}
                            </textarea>

                            @if ($errors->has('projectValue'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('projectValue') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('3.Opis Projekta ') }}</div>
                            <br><br>
                            <textarea id="projectDescription" type="text" class="form-control{{ $errors->has('projectDescription') ? ' is-invalid' : '' }}" name="projectDescription" value="" required autofocus>{{ $singleProject->projectDescription }}</textarea>

                            @if ($errors->has('projectDescription'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('projectDescription') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('4.Opis Kompanije (datum osnivanja, broj zaposlenih, aktiva, poslovni bilansi, pozicija na tržištu razvojne perspektive,  
                                potencijalna izvozna orijentacija, poslovni partneri) ') }}</div>
                            <br><br>
                            <textarea id="companyDescr" type="text" class="form-control{{ $errors->has('companyDescr') ? ' is-invalid' : '' }}" name="companyDescr" value="" required autofocus>{{ $upit[0]->companyDescr}}</textarea>

                            @if ($errors->has('companyDescr'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('companyDescr') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('5.Opis regiona u kome se nalazi Projekat (za mape 1 i 2)') }}</div>
                            <br><br>
                            <textarea id="regionDescr" type="text" class="form-control{{ $errors->has('regionDescr') ? ' is-invalid' : '' }}" name="regionDescr" value="" required autofocus>{{ $upit[0]->regionDescr }}</textarea>

                            @if ($errors->has('regionDescr'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('regionDescr') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('6.Menadžer projekta') }}</div>
                                <br><br>
                                <textarea id="projectManager" type="text" class="form-control{{ $errors->has('projectManager') ? ' is-invalid' : '' }}" name="projectManager" value="" required autofocus>{{ $singleProject->projectManager}}</textarea>

                                @if ($errors->has('projectManager'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('projectManager') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <div class="inputText">{{ __('7.Pravna regulativa (sertifikati, atesti, zaštitita patentnog prava, zemljišno vlasništvo i dr.)') }}</div>
                                <br><br>
                                <textarea id="legalRegulations" type="text" class="form-control{{ $errors->has('legalRegulations') ? ' is-invalid' : '' }}" name="legalRegulations" value="" required autofocus>{{ $upit[0]->legalRegulations}}</textarea>

                                @if ($errors->has('legalRegulations'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('legalRegulations') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <div class="inputText">{{ __('8.Potencijal projekta (šta se konkretno traži od strane predlagača projekta)') }}</div>
                                <br><br>
                                <textarea id="projectPotencial" type="text" class="form-control{{ $errors->has('projectPotencial') ? ' is-invalid' : '' }}" name="projectPotencial" value="" required autofocus>{{ $upit[0]->projectPotencial}}</textarea>

                                @if ($errors->has('projectPotencial'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('projectPotencial') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <div class="inputText">{{ __('9.Kontakt informacije (puna informacija koja postoji: web-site, mail, T/F, adresa)') }}</div>
                                <br><br>
                                <textarea id="contactData" type="text" class="form-control{{ $errors->has('contactData') ? ' is-invalid' : '' }}" name="contactData" value="" required autofocus>{{ $singleProject->contactData}}</textarea>

                                @if ($errors->has('contactData'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('contactData') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <div class="form-group">
                                    <label for="#">{{ __('OBLAST POSLOVANJA') }}</label>
                                    <br/>
                                    @if($upit[0]->businessArea)
                                    <h5>Izabrana oblast poslovanja je : 
                                    {{$upit[0]->businessArea}}
                                    </h5>
                                    @else 
                                    <p>There is no selected business area</p>
                                    @endif
                                    <br/>
                                    <select name="businessArea" multiple class="form-control" id="businessArea" value="{{ $singleProject->businessArea}}">
                                    @foreach($oblasts as $oblast)
                                    <option value="{{$oblast->poslovnaOblast}}">{{$oblast->poslovnaOblast}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {{--'scaleValue', --}}
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <div class="form-group">
                                    <label for="#">{{ __('SKALA VREDNOSTI PROJEKTA') }} {{$scaleValue}}</label>
                                    <select name="scaleValue" class="form-control" id="scaleValue">
                                        <option {{ $scaleValue == 'Do 50.000' ? 'selected' : ''}} value="Do 50.000">Do 50.000 &#8364;</option>
                                        <option {{ $scaleValue == 'Od 50.001–100.000' ? 'selected' : ''}} value="Od 50.001–100.000">Od 50.001 – 100.000 &#8364;</option>
                                        <option {{ $scaleValue == 'Od 100.001–500.000' ? 'selected' : ''}} value="Od 100.001–500.000">Od 100.001 – 500.000 &#8364;</option>
                                        <option {{ $scaleValue == 'Od 500.001–1.000.000' ? 'selected' : ''}} value="Od 500.001–1.000.000">Od 500.001 – 1.000.000 &#8364;</option>
                                        <option {{ $scaleValue == 'Od 1 mil–5 mil' ? 'selected' : ''}} value="Od 1 mil–5 mil">Od 1 mil – 5 mil &#8364;</option>
                                        <option {{ $scaleValue == 'Od 5 mil–10 mil' ? 'selected' : ''}} value="Od 5 mil–10 mil">Od 5 mil – 10 mil &#8364;</option>
                                        <option {{ $scaleValue == 'Od 10 mil–50 mil' ? 'selected' : ''}} value="Od 10 mil–50 mil">Od 50 mil &#8364; - 100 mil &#8364;</option>
                                        <option {{ $scaleValue == 'Od 50 mil-100 mil' ? 'selected' : ''}} value="Od 50 mil-100 mil">Od 50 mil &#8364; - 100 mil &#8364;</option>
                                        <option {{ $scaleValue == 'Od 100 mil–500 mil' ? 'selected' : ''}} value="Od 100 mil–500 mil">Od 100 mil – 500 mil &#8364;</option>
                                        <option {{ $scaleValue == 'Preko 500 mil' ? 'selected' : ''}} value="Preko 500 mil">Preko 500 mil &#8364;</option>
                                    </select>
                                </div>
                                <div>
                                </div>
                            </div>
                        </div>
                    </div>

            {{-- <input type="hidden" name="slug" value="{{$availableProject->slug}}"> --}}
                
                <div class="row">
                    <div class="col-sm-12">
                        <input type="submit" name="#" class="button" value="{{ __('Send Information / Pošalji Informacije') }}">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Start Script-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Main Script -->
{{--  <script src="js/main.js" type="text/javascript"></script>  --}}
<!-- / Script-->

@endsection
