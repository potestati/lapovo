<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Forma apliciranja za projekat">
    <meta name="keywords" content="Forma apliciranja za projekat">
    <meta name="author" content="Forma apliciranja za projekat">
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
    <link rel="icon" href="img/favicon.png" type="image/x-icon">
    <title>GIM</title>
    <!-- Bootstrap CSS -->
    {{--  <link href="css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">  --}}
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
    <!--  GOOGLE MAP -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"/>
    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js" ></script>
    <!--  Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}">
</head>
<body>
    <div id="app">
        @include('inc.navbar')
        <br><br>
        <h1 style="text-align: center;">{{ __('messages.GIM') }} </h1>
        <br><br>
        <hr>
        <div class="container register">
          @yield('content')
        </div>
    </div>
<!-- Footer -->
<footer class="page-footer font-small unique-color-dark">

    <div style="background-color: #6351ce;">
      <div class="container">

        <!-- Grid row-->
        <div class="row py-4 d-flex align-items-center">

          <!-- Grid column -->
          <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
            <h6 class="mb-0">{{ __('messages.social') }}</h6>
          </div>
          <!-- Grid column -->
          <div class="col-md-6 col-lg-7 text-center text-md-right">
            <!-- Facebook -->
            <a href="https://www.facebook.com/usluzbigradjana/" class="fb-ic">
            <i class="fa fa-facebook-official" aria-hidden="true"></i>
            </a>
            <!-- Twitter -->
            <a href="https://twitter.com/opstinalapovo" class="tw-ic">
            <i class="fa fa-twitter-square" aria-hidden="true"></i>
            </a>
            <!-- Google +-->
            <a class="gplus-ic">
            <i class="fa fa-google-plus-official" aria-hidden="true"></i>
            </a>
            <!--Linkedin -->
            <a class="li-ic">
            <i class="fa fa-linkedin-square" aria-hidden="true"></i>
            </a>
            <!--Instagram-->
            <a class="ins-ic">
            <i class="fa fa-instagram" aria-hidden="true"></i>
            </a>

          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row-->

      </div>
    </div>

    <!-- Footer Links -->
    <div class="container text-center text-md-left mt-5">

      <!-- Grid row -->
      <div class="row mt-3">

        <!-- Grid column -->
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <!-- Content -->
          <h6 class="text-uppercase font-weight-bold">{{ __('messages.supervizor') }}</h6>
          <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p>Mirela Radenković, {{ __('messages.pomoćnik') }}</p>
          <p>Email: <a href="mailto:office@lapovo.rs">office@lapovo.rs</a></p>
        </div>
        <!-- Grid column -->
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
        </div>
        <!-- Grid column -->
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
        </div>
        <!-- Grid column -->
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
          <!-- Links -->
          <h6 class="text-uppercase font-weight-bold">{{ __('messages.Kontakt') }}</h6>
          <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p>
          <i class="fa fa-map-marker" aria-hidden="true"></i><a href="https://goo.gl/maps/fq6sgPbbRJX5LV1u8"> {{ __('messages.Adresa') }}</a></p>
          <p>
          <i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:office@lapovo.rs"> office@lapovo.rs</a></p>
          <p>
          <i class="fa fa-phone" aria-hidden="true"></i><a href="tel:+381-34-853-159"> +381 34 853 159</p>
          <!-- <p>
          <i class="fa fa-mobile" aria-hidden="true"></i> + 01 234 567 89</p> -->
        </div>
        <!-- Grid column -->
      </div>
      <!-- Grid row -->
    </div>
    <!-- Footer Links -->
    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2019 Copyright:
    <a href="http://www.lapovo.rs/main/">{{ __('messages.Opština') }} Lapovo</a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- Footer -->
    <!-- Popper JS -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script type="text/javascript">
        $(".input").focus(function () {
            $(this).parent().addClass("focus");
        })
    </script>
    <script>
    $(document).ready(function() {
        $("#news-slider").owlCarousel({
            items : 2,
            itemsDesktop : [1199,2],
            itemsMobile : [600,1],
            pagination :true,
            autoPlay : true
        });
        $("#news-slider9").owlCarousel({
            items : 3,
            itemsDesktop:[1199,2],
            itemsDesktopSmall:[980,2],
            itemsTablet:[650,1],
            pagination:false,
            navigation:true,
            navigationText:["",""]
        });

        console.log("document ready");
        if (typeof sveSpremnoZaMape === "function") {
            console.log("document ready 2");
            sveSpremnoZaMape();
        }

    });
    $( '.navbar-nav a' ).on( 'click', function () {
        $( '.navbar-nav' ).find( 'li.active' ).removeClass( 'active' );
        $( this ).parent( 'li' ).addClass( 'active' );
    });
    </script>
    <script src="{{ asset('/js/main.js') }}"></script> 
</body>
</html>
