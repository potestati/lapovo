@extends('layouts.dashboard')

@section('content')

@include('inc.sidebar')
    <div class="col-sm-9 dashboard-report-table">
        <div class="main">

         <form method="POST" action="#" novalidate>
                @csrf
            <!-- <div style="overflow-x:auto;"> -->
            <table style="overflow-x:auto;" class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Naziv projekta</th>
                        <th scope="col">Status projekta</th>
                        <th scope="col">Vrednost projekta</th>
                        <!--<th scope="col">Kategorija projekta</th>
                        <th scope="col">Oblast projekta</th>-->
                    </tr>
                </thead>
                <tbody>
                    @foreach ($approvedProjects as $index=>$project)
                        <tr>
                            <th scope="row">{{++$index}}</th>
                            <td>{{$project->projectName}}</td>
                            <td>
                                @if($project->status == 0)
                                U procesu
                                @elseif($project->status == 1)
                                Odobren
                                @elseif($project->status == 2)
                                Odbijen
                                @elseif($project->status == 3)
                                Realizovan
                                @endif
                            </td>
                        @foreach($approvedProjects as $approvedProject)
                            @if($approvedProject->id == $project->id)
                            <i style="display: none;">
                                {{$exactValue = $approvedProject->exact_value}}
                                {{$projectValue = number_format($exactValue, 2, ',', ' ')}}
                            </i>
                            <td>{{$projectValue}} EUR</td>
                            </tr>
                            @endif
                        @endforeach       
                    @endforeach
                </tbody>
            </table>
            <!-- <input type="submit" value="Submit"> -->
           <!-- <div>  -->
        </form>
        </div>
    </div>
    <!-- Start Script-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
       
    <!-- Popper JS -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    {{--  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>  --}}
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Main Script -->
    <script src="js/main.js" type="text/javascript"></script>
        
@endsection
